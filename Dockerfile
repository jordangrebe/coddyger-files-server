FROM node:latest
# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install  dependencies
COPY package*.json ./
RUN pnpm install
# If you are building your code for production
# RUN npm ci --only=production
COPY . .
COPY .env.sample .env
# Add port
EXPOSE 3000
CMD [ "npm","start", "./build/main.js" ]
