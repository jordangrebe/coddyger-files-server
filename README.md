# CoddyMag REST API

![version](https://img.shields.io/badge/version-1.0.5-blue.svg)
![node](https://img.shields.io/badge/node-%3E%3D18.16.0-brightgreen.svg)
![npm](https://img.shields.io/badge/npm-%3E%3D9.5.1-brightgreen.svg)
![license](https://img.shields.io/badge/license-MIT-blue.svg)

## Description

The Coddyger REST API is a robust and efficient backend solution designed to power modern web applications. Built with Node.js and Express, this API provides a scalable and secure platform for handling various tasks, such as user authentication, data validation, and database interactions. It comes with a range of features, including support for different database systems, file uploads, email notifications, and API documentation using Swagger. Whether you're building a small-scale application or a large-scale enterprise system, the Coddyger REST API is designed to meet your needs with flexibility and performance in mind.

## Prerequisites

- Node.js >= 20.11.1
- npm >= 10.2.4

## Installation

1. Clone the repository
2. Install dependencies with `pnpm install`

## Usage

- Run in development mode: `npm run dev`
- Start the API: `npm start`
- Run tests: `npm test`
- Deploy using Docker: `npm run deploy`
- Run Docker container: `npm run-container`
- Create new module: `pnpm create-module`
