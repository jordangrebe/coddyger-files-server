module.exports = function (plop) {
	plop.setGenerator('module', {
    description: 'Générer un module',
    prompts: [{
      type: 'input',
      name: 'name',
      message: 'Nom du module:',
    }],
    actions: [
			{
				type: 'add',
				path: './src/modules/{{name}}/index.ts',
				templateFile: 'templates/no-user/index.hbs',
			},
			{
				type: 'add',
				path: './src/modules/{{name}}/{{name}}.model.ts',
				templateFile: 'templates/no-user/model.hbs',
			},
			{
				type: 'add',
				path: './src/modules/{{name}}/{{name}}.controller.ts',
				templateFile: 'templates/no-user/controller.hbs',
    	},
			{
				type: 'add',
				path: './src/modules/{{name}}/{{name}}.service.ts',
				templateFile: 'templates/no-user/service.hbs',
			},
			{
				type: 'add',
				path: './src/routes/{{name}}.route.ts',
				templateFile: 'templates/no-user/route.hbs',
			},
		],
  });
	plop.setGenerator('module-user', {
    description: 'Générer un module avec contrôle utilisateur',
    prompts: [{
      type: 'input',
      name: 'name',
      message: 'Nom du module:',
    }],
    actions: [
			{
				type: 'add',
				path: './src/modules/{{name}}/index.ts',
				templateFile: 'templates/with-user/index.hbs',
			},
			{
				type: 'add',
				path: './src/modules/{{name}}/{{name}}.model.ts',
				templateFile: 'templates/with-user/model.hbs',
			},
			{
				type: 'add',
				path: './src/modules/{{name}}/{{name}}.controller.ts',
				templateFile: 'templates/with-user/controller.hbs',
    	},
			{
				type: 'add',
				path: './src/modules/{{name}}/{{name}}.service.ts',
				templateFile: 'templates/with-user/service.hbs',
			},
			{
				type: 'add',
				path: './src/routes/{{name}}.route.ts',
				templateFile: 'templates/with-user/route.hbs',
			},
		],
  });
	plop.setGenerator('route', {
    description: 'Généner une route API pour un module',
    prompts: [{
      type: 'input',
      name: 'name',
      message: 'Nom de la route (nom du module):',
    }],
    actions: [
			{
				type: 'add',
				path: './src/routes/{{name}}.route.ts',
				templateFile: 'templates/route.hbs',
			},
		],
  });
	plop.setGenerator('model', {
    description: 'Généner un modèle',
    prompts: [{
      type: 'input',
      name: 'name',
      message: 'Nom du modèle:',
    }],
    actions: [
			{
				type: 'add',
				path: './src/commons/models/{{name}}.model.ts',
				templateFile: 'templates/no-user/model.hbs',
			},
		],
  });
};
