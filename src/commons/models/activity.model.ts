import mongoose, { Schema, Document } from 'mongoose';
import coddyger, { IData, IErrorObject, defines, LoggerService, LogLevel, MongoDbDao } from 'coddyger'

export interface IActivity {
	_id?: string; // MongoDB generated id
	slug?: string; // Reference of the document
	title?: string; // Title of the document
	content?: string; // Content of the document
	status?: string; // Status of the document - active - removed - archived
	item?: any; // L'objet concerné par l'activité
	itemType: string; // Champ dynamique pour indiquer le type de l'objet référencé
	user?: any; // Owner of the document
}

const schema = new mongoose.Schema<IActivity>(
	{
		_id: Schema.Types.ObjectId,
		slug: String,
		title: String,
		content: String,
		status: { type: String, enum: ['active', 'archived', 'removed'], default: 'active' },
		item: { type: Schema.Types.ObjectId, refPath: 'itemType' },
		itemType: { type: String, required: true },
		user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
	},
	{ timestamps: true }
);

const model = mongoose.model<IActivity>('Activity', schema);

export class ActivitySet extends MongoDbDao<Document> implements IData<Document> {
	defaultModel = model;

	constructor() {
		super();
	}

	props: string = 'slug';
	userProps: string = 'slug email lastname firstname';
	setTitle: string = 'ActivitySet';

	select(payloads: { 
		params?: any, 
		excludes?: string, 
		page?: number, 
		pageSize?: number,
		sort?: string,
		orderBy?: string; 
	}): Promise<any> {
		return new Promise(async (resolve, reject) => {
			if (coddyger.string.isEmpty(payloads.params.status)) {
				payloads.params = { ...payloads.params, status: { $nin: ['removed', 'archived'] } };
			}

			let page: number = Number(payloads.page) || 1;
			const pageSize: number = Number(payloads.pageSize) || 10;

			page = page === 0 ? 1 : page;

			const startIndex = (page - 1) * pageSize;
			const model = this.defaultModel;
			let sortBy: string = payloads.sort ?? 'createdAt';
			let orderBy: string = payloads.orderBy ?? 'desc';

			// Create sort object
			let sortObject: any = {};
			if (sortBy) {
				sortObject = {
					[sortBy]: orderBy === 'desc' ? -1 : 1
				};
			} else {
				// Default sort by createdAt in descending order if no sortBy is provided
				sortObject = { createdAt: -1 };
			}

			const [rows, totalRows] = await Promise.all([
				model
					.find(payloads.params, "-password -__v")
					.sort(sortObject)
					.skip(startIndex)
					.limit(pageSize)
					.lean(),
				model.countDocuments(payloads.params)
			]);

			if (rows) {
				const totalPages = Math.ceil(totalRows / pageSize);
				const countRowsPerPage = rows.length;
				const totalPagesPerQuery = Math.ceil(totalRows / pageSize);

				resolve({
					rows,
					totalRows,
					totalPages,
					countRowsPerPage,
					totalPagesPerQuery
				});
			} else {
				reject({ rows, totalRows });
			}
		}).catch((e: any) => {
			LoggerService.log({
				type: LogLevel.Error,
				content: JSON.stringify(e),
				location: this.setTitle,
				method: 'select'
			});
			return { error: true, data: e, message: defines.message.tryCatch };
		});
	}

	selectHug(): Promise<Array<Document> | any> {
		return new Promise(async (resolve, reject) => {
			let doc = await this.defaultModel.find().lean().populate({
				path: 'user',
				select: this.userProps,
				options: { sort: { createdAt: -1 } }
			}).populate('itemType')

			if (!doc) {
				reject(doc);
			} else {
				resolve(doc);
			}
		}).catch((e: any) => {
			LoggerService.log({ type: LogLevel.Error, content: e, location: this.setTitle, method: 'selectHug' });
			return { error: true, data: e, message: defines.message.tryCatch };
		});
	}

	selectOne(params: object, fields?: string): Promise<Document | any> {
		return new Promise(async (resolve, reject) => {
			resolve(
				await this.defaultModel.findOne(params, fields).lean().populate({
					path: 'user',
					select: this.userProps,
					options: { sort: { createdAt: -1 } }
				}).populate('itemType')
			);
		}).catch((e: any) => {
			LoggerService.log({ type: LogLevel.Error, content: e, location: this.setTitle, method: 'selectOne' });
			return { error: true, data: e, message: defines.message.tryCatch };
		});
	}
}
