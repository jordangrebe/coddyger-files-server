import multer, { StorageEngine, FileFilterCallback } from 'multer';
import path from 'path';
import fs from 'fs';
import { Request, Response, NextFunction } from 'express';
import coddyger, { defines } from 'coddyger';

const uploadPath: any = path.join(coddyger.root() + '/src/public/uploads/');

class FileHelper {
	private uploadDirectory: string;
	private maxSize: number;
	private fileTypes: string[];
	private storage: StorageEngine;
	private upload: multer.Multer;

	constructor(uploadDirectory: string, maxSize: number, fileTypes: string[]) {
		this.uploadDirectory = uploadPath + uploadDirectory;
		this.maxSize = maxSize;
		this.fileTypes = fileTypes;

    this.buildUploadPath(this.uploadDirectory)

		// Set up Multer storage configuration
		this.storage = multer.diskStorage({
			destination: (req, file, cb) => {
				cb(null, this.uploadDirectory);
			},
			filename: (req, file, cb) => {
				const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
				cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname));
			}
		});

		// Set up Multer upload configuration
		this.upload = multer({
			storage: this.storage,
			limits: { fileSize: this.maxSize },
			fileFilter: (req, file, cb) => {
				this.fileFilter(req, file, cb);
			}
		}).single('file'); // Change 'file' to match your form field name
	}

	private fileFilter(req: Request, file: any, cb: FileFilterCallback) {
		const ext = path.extname(file.originalname).toLowerCase();
		if (!this.fileTypes.includes(ext)) {
			return cb(new Error('Type de fichier non autorisé'));
		}
		cb(null, true);
	}

	public uploadFile(req: Request, res: Response, next: NextFunction) {
		this.upload(req, res, (err) => {
			if (err instanceof multer.MulterError) {
        if(err.code === 'LIMIT_FILE_SIZE') {
          err.message = 'Fichier trop volumineux'
        } else if(err.code === 'LIMIT_FILE_COUNT') {
          err.message = 'Fichier trop volumineux'
        }

				return res.status(defines.status.clientError).json({ message: err.message });
			} else if (err) {
				return res.status(defines.status.clientError).json({ message: err.message });
			}
			next();
		});
	}

  async buildUploadPath(payload:string) {
		// CREATE UPLOAD PATH
		if (!fs.existsSync(payload)) {
      await fs.promises.mkdir(uploadPath + '/.tmp', { recursive: true });
			await fs.promises.mkdir(payload, { recursive: true });
		}
	}

  static save(file: any) {
		return new Promise((resolve, reject) => {
			const tempPath = file.path;
      let fullFilePath = uploadPath + file.filename;
      let filename: string = file.filename;
      
      fs.rename(tempPath, fullFilePath, (err) => {
        if (err) reject(err);

        resolve({ filename, fullFilePath });
      });
		}).catch((error) => {
			return { error: true, data: error };
		});
	}
}

export default FileHelper;
