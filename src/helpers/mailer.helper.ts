import nodemailer from 'nodemailer'
import coddyger, { env } from 'coddyger';
import fs from 'fs';
import util from 'util';
import path from 'path';

// Create a transporter with your SMTP configuration
const transporter = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: 587,
  secure: false,
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASSWORD
  },
	tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false,
  },
});

export class MailerHelper {
	static async send(payloads: { from?: string; to: Array<string>; subject: string; text?: string; html?: string }) {
		// Setup email data
		const mailOptions = {
			from: payloads.from || 'ultrondev@ultrondev.com',
			to: payloads.to,
			subject: payloads.subject,
			text: payloads.text,
			html: payloads.html || payloads.text
		};

		// Send the email
		transporter.sendMail(mailOptions, (error, info) => {
			if (error) {
				console.error('Error occurred: ', error.message);
				return;
			}
			console.log('Message sent: %s', info.messageId);
		});
	}

	static verify(): void {
		transporter.verify(function (error, success) {
			if (error) {
				console.log(error);
			} else {
				coddyger.konsole('Serveur mail connecté!');
			}
		});
	}

	static buildCredentialTemplate(name:string, email:string, password:string) {
		return new Promise((resolve, reject) => {
			// Read the HTML template file
			const readFile = util.promisify(fs.readFile);
			
			readFile(path.join(coddyger.root() + '/src/public/mail-template/mail-credential.html'), 'utf8')
			.then((template) => {
				resolve(template.replace('{{name}}', name)
				.replace('{{email}}', email)
				.replace('{{email}}', email)
				.replace('{{password}}', password)
				.replace('{{appName}}', env.appName)
				.replace('{{appName}}', env.appName)
				.replace('{{appName}}', env.appName)
				.replace('{{supportEmail}}', env.domain)
			)
			}).catch((error:any) => {
				reject(error)
			});
		}).catch((error:any) => {
			coddyger.konsole('Impossible de lire le fichier mail-credential');
			return { error: true, data: error}
		})
	}

	static buildWelcomeTemplate(email:string) {
		return new Promise((resolve, reject) => {
			// Read the HTML template file
			const readFile = util.promisify(fs.readFile);
			
			readFile(path.join(coddyger.root() + '/src/public/mail-template/mail-welcome.html'), 'utf8')
			.then((template) => {
				resolve(template.replace('{{email}}', email)
				.replace('{{email}}', email)
				.replace('{{appName}}', env.appName)
				.replace('{{appName}}', env.appName)
				.replace('{{appName}}', env.appName)
				.replace('{{supportEmail}}', env.domain)
			)
			}).catch((error:any) => {
				reject(error)
			});
		}).catch((error:any) => {
			coddyger.konsole('Impossible de lire le fichier mail-welcome');
			return { error: true, data: error}
		})
	}
}
