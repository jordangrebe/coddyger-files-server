import coddyger, { IData, IErrorObject } from 'coddyger'
import { ActivitySet, IActivity } from '../commons/models';

const daoActivity: IData<IActivity> = new ActivitySet();

export class MainHelper {
	static saveActivity(payload: {user: string, item: any, nouvelleConsommation: number, nombreMois?: number}) {
		return new Promise(async (resolve, reject) => {
  		const itemModelName = payload.item.constructor.name;

			await daoActivity.save({
				_id: coddyger.string.generateObjectId(),
				user: payload.user,
				item: payload.item._id,
				itemType: itemModelName
			})

			resolve(true)
		}).catch((err: any) => {
			return {};
		});
	}
}
