import { Server } from 'socket.io';

export class SocketIOHelper {
	private io: Server;

	constructor(io: any) {
		this.io = io;

		this.run();
	}

	private run(): void {
		this.io.on('connection', (socket) => {
			console.log(`Socket connected: ${socket.id}`);
			socket.emit('welcome', 'Connected to server');
			socket.emit('refresh-page', true);

			socket.on('start-activation', (data) => {
				console.log(`Received start-activation: ${data}`);

				socket.emit('responseEvent', 'Response from server');
			});

			socket.on('chat-message', (data) => {
				console.log(`Received chat-message: ${data}`);

				socket.emit('responseEvent', 'Response from server');
			});

			socket.on('disconnect', () => {
				console.log(`Socket disconnected: ${socket.id}`);
			});
		});
	}

	public transactionDone(payload: string): void {
		this.io.on('connection', (socket) => {
			console.log(`Socket connected: ${socket.id}`);

			socket.emit('tx-done-' + payload, { message: 'Welcome to the server!' });

			socket.on('disconnect', () => {
				console.log(`Socket disconnected: ${socket.id}`);
			});
		});
	}
}
