import jwt from 'jsonwebtoken';
import coddyger, { defines, env } from 'coddyger';
import { TokenSet } from '../commons';
import { UserSet } from '../modules/user';
import { locale } from '../public';

export class TokenHelper {
	private static readonly daoToken:any = new TokenSet
	private static readonly daoUser:any = new UserSet

	private static secretKey:string = env.jwt.secret!
	private static secretAuthKey:string = env.jwt.secretAuth!

	static async verify(req: any, res: any, next: any) {
		const authorization = req.headers.authorization;

		if(authorization) {
			let token:string = authorization.split(' ')[1];

			const isUsedToken = await TokenHelper.isUsedToken(token);

			if (isUsedToken) {
				return coddyger.api(
					res,
					new Promise((resolve) => {
						resolve({
							status: defines.status.authError,
							message: 'Votre session a expiré',
							data: 'session_revocation'
						});
					})
				);
			} else {
				jwt.verify(token, TokenHelper.secretKey, (err: any, user: any) => {
					if (err) {
						const errName = err.name;
						let errLabel: string;
						if (errName === 'TokenExpiredError') {
							errLabel = 'Votre session a expiré';
						} else if (errName === 'JsonWebTokenError') {
							errLabel = 'Signature du token invalide';
						} else {
							errLabel = err;
						}
						return coddyger.api(
							res,
							new Promise((resolve) => {
								resolve({
									status: defines.status.authError,
									message: errLabel,
									data: null
								});
							})
						);
					}

					req.user = user;
					next();
				});
			}
		} else {
			return coddyger.api(
				res,
				new Promise((resolve) => {
					resolve({
						status: defines.status.authError,
						message: "Vous n'avez pas les autorisations nécessaires pour accéder à cette ressource",
						data: 'no_auth'
					});
				})
			);
		}
	}

	static async verifyAdmin(req: any, res: any, next: any) {
		const token = TokenHelper.builtToken(req.headers.authorization);
		const isUsedToken = await TokenHelper.isUsedToken(token);

		if (isUsedToken) {
			return coddyger.api(
				res,
				new Promise((resolve) => {
					resolve({
						status: defines.status.authError,
						message: 'Votre session a expiré',
						data: 'session_revocation'
					});
				})
			);
		} else {
			jwt.verify(token, TokenHelper.secretKey!, async (err: any, user: any) => {
				if (!user) {
					return coddyger.api(
						res,
						new Promise((resolve) => {
							resolve({
								status: defines.status.authError,
								message: locale.controller.notAuthorized,
								data: null
							});
						})
					);
				}

				const _id: string = user._id;
				const isAgent: any = await TokenHelper.daoUser.exist({ _id });

				if (!isAgent) {
					return coddyger.api(
						res,
						new Promise((resolve) => {
							resolve({
								status: defines.status.authError,
								message: locale.controller.notAdminAccount,
								data: null
							});
						})
					);
				} else {
					if (err) {
						const errName = err.name;
						let errLabel: string;
						if (errName === 'TokenExpiredError') {
							errLabel = 'Votre session a expiré';
						} else if (errName === 'JsonWebTokenError') {
							errLabel = 'Signature du token invalide';
						} else {
							errLabel = err;
						}
						return coddyger.api(
							res,
							new Promise((resolve) => {
								resolve({
									status: defines.status.authError,
									message: errLabel,
									data: errLabel
								});
							})
						);
					}

					req.user = user;
					next();
				}
			});
		}
	}

	static async verifyAuth(req: any, res: any, next: any) {
		const authorization = req.headers.authorization;
		if(authorization) {
			let token:string = authorization.split(' ')[1];

			jwt.verify(token, TokenHelper.secretAuthKey!, (err: any, user: any) => {
				if (err) {
					const errName = err.name;
					let errLabel: string;
					if (errName === 'TokenExpiredError') {
						errLabel = 'Votre session a expiré';
					} else if (errName === 'JsonWebTokenError') {
						errLabel = 'Signature du token invalide';
					} else {
						errLabel = err;
					}
					return coddyger.api(
						res,
						new Promise((resolve) => {
							resolve({
								status: defines.status.authError,
								message: errLabel,
								data: null
							});
						})
					);
				}

				req.user = user;
				next();
			});
		} else {
			return coddyger.api(
        res,
        new Promise((resolve) => {
          resolve({
            status: defines.status.authError,
            message: "Vous n'avez pas les autorisations nécessaires pour accéder à cette ressource",
            data: 'no_auth'
          });
        })
      );
		}
	}

	static revock(req: any, res: any, next: any) {
		const authHeader = req.headers.authorization;

		if (authHeader) {
			const token = authHeader.split(' ')[1];

			TokenHelper.daoToken.exist({ hash: token }).then((a: any) => {
				if (a.error) {
					return a;
				} else {
					if (a === true) {
						return coddyger.api(
							res,
							new Promise((resolve) => {
								resolve({
									status: defines.status.authError,
									message: 'Votre session a expiré',
									data: 'session_revocation'
								});
							})
						);
					} else {
						jwt.verify(token, TokenHelper.secretKey!, (err: any, user: any) => {
							if (err) {
								const errName = err.name;
								let errLabel: string;
								if (errName === 'TokenExpiredError') {
									errLabel = 'Votre session a expiré';
								} else if (errName === 'JsonWebTokenError') {
									errLabel = 'Signature du token invalide';
								} else {
									errLabel = err;
								}
								return coddyger.api(
									res,
									new Promise((resolve) => {
										resolve({
											status: defines.status.authError,
											message: errLabel,
											data: errLabel
										});
									})
								);
							}

							req.user = user;
							next();
						});
					}
				}
			});
		} else {
			return coddyger.api(
				res,
				new Promise((resolve) => {
					resolve({
						status: defines.status.authError,
						message: locale.controller.notAuthorized,
						data: ''
					});
				})
			);
		}
	}

	static generate(data: any, expiresIn?: any) {
		// generate an access token - exp: Math.floor(Date.now() / 1000) + (60 * 60)
		let accessToken: string = jwt.sign(data, TokenHelper.secretKey!, { expiresIn: "720m" });
		let refreshToken: string = jwt.sign(data, TokenHelper.secretKey!, { expiresIn: "142h" });

		return { accessToken, refreshToken };
	}

	static generateAuth(data: {}) {
		return jwt.sign(data, TokenHelper.secretAuthKey, { expiresIn: '720m' });
	}

	private static async isUsedToken(token: string) {
		const isToken: any = await TokenHelper.daoToken.exist({ hash: token });

		return isToken;
	}

	private static builtToken(authorization: string): string {
		return authorization.includes("Bearer") ? authorization.split(' ')[1] : "";
	}
}
