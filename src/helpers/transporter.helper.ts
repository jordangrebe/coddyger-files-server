import { TransporterService, env } from 'coddyger';

let x = 0;
let topics: Array<any> = [];
let sets: Array<any> = [];

let transporterForeigners: Array<any> = env.transporter.foreigners.split(',');

if (env.transporter.foreigners) {
	transporterForeigners = env.transporter.foreigners.split(',');
}

transporterForeigners.forEach((item: any) => {
	topics.push({
		id: item,
		set: sets[x] || null
	});

	x++;
});

export class TransporterHelper extends TransporterService {
	constructor() {
		let finalTopics: Array<any> = [];

		for (let x in topics) {
			let topic: any = topics[x];
			let topicSet: any = topic.set;

			finalTopics.push(topic);
		}

		super(finalTopics);
	}

	save(msg: any) {
		msg = JSON.stringify(msg);

		this.send(msg).catch((err) => {
			console.log(err)
		});
	}

	sendMail(msg: any) {
		msg = JSON.stringify(msg);

		this.send(msg, 'sendmail-topic').catch((err) => {
			console.log(err)
		});
	}

	sendSms(msg: any) {
		msg = JSON.stringify(msg);

		this.send(msg, 'sendsms-topic').catch((err) => {
			console.log(err)
		});
	}

	getMessage(topic: any) {
		this.consumer.connect().then(() => {
			// Start consumer
			this.consumer.run({
				eachMessage: async (payload: { topic: any; partition: any; message: any }) => {
					if (topic === payload.topic) {
						let msg = {
							partition: payload.partition,
							offset: payload.message.offset,
							value: payload.message.value.toString()
						};
						console.log('CONSUMING ::', payload.topic);
						let data = JSON.parse(msg.value);
					}
				}
			});
		});
	}
}
