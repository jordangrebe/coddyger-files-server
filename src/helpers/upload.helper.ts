import multer from 'multer';
import path from 'path';
import fs from 'fs';
import mime from 'mime-types';
import coddyger, { defines } from 'coddyger';

// NATS SERVER
export class UploadHelper {
	static uploadPathTmp: any = path.join(coddyger.root() + '/src/public/uploads/.tmp');
	static uploadPath: any = path.join(coddyger.root() + '/src/public/uploads/');
	static maxFile = 10;
	static fileSize = 1000000 * 10;

	static async single(req: any, res: any, next: any) {
		await UploadHelper.buildUploadPath();
		let allowedExtension = ['pdf', 'xls', 'xlsx', 'docx', 'doc', 'png', 'jpg', 'jpeg', 'mp3', 'wave'];
		const upload = multer({
			dest: UploadHelper.uploadPathTmp,
			limits: {
				fileSize: UploadHelper.fileSize
			},
			fileFilter: (req: any, file: any, cb: any) => {
				if (!coddyger.inArray(UploadHelper.buildExt(file.mimetype), allowedExtension)) {
					req.fileValidationError = 'Type de fichier non autorisé';
					return coddyger.api(
						res,
						new Promise((resolve) => {
							resolve({
								status: defines.status.clientError,
								message: 'Type de fichier non autorisé',
								data: null
							});
						})
					);
				} else {
					console.log('aaaaaaaaaaaaaaaaaaaaa')
				}
				cb(null, true);
			}
		}).single('file');

		upload(req, res, function (err: any) {
			if (req.file) {
				if (err instanceof multer.MulterError) {
					if (err.code === 'LIMIT_FILE_SIZE') {
						err.message = 'Fichier trop volumineux';
					} else if (err.code === 'LIMIT_FILE_COUNT') {
						err.message = 'Nombre maximum de fichier atteint';
					}
					return coddyger.api(
						res,
						new Promise((resolve) => {
							resolve({
								status: defines.status.clientError,
								message: err.message,
								data: null
							});
						})
					);
				} else if (req.fileValidationError) {
					return coddyger.api(
						res,
						new Promise((resolve) => {
							resolve({
								status: defines.status.clientError,
								message: 'Type de fichier non autorisé',
								data: null
							});
						})
					);
				} else if (err) {
					return coddyger.api(
						res,
						new Promise((resolve) => {
							resolve({
								status: defines.status.clientError,
								message: err.message,
								data: null
							});
						})
					);
				}
			} else {
				req.file = '';
			}

			next();
		});
	}
	static async multiple(req: any, res: any, next: any) {
		await UploadHelper.buildUploadPath();
		let allowedExtension = ['pdf', 'xls', 'xlsx', 'docx', 'txt', 'doc', 'png', 'jpg', 'jpeg', 'pptx'];
		const upload = multer({
			dest: UploadHelper.uploadPathTmp,
			limits: {
				fileSize: UploadHelper.fileSize,
				files: UploadHelper.maxFile
			},
			fileFilter: (req: any, file: any, cb: any) => {
				if (!coddyger.inArray(UploadHelper.buildExt(file.mimetype), allowedExtension)) {
					return coddyger.api(
						res,
						new Promise((resolve) => {
							resolve({
								status: defines.status.clientError,
								message: 'Type de fichier non autorisé',
								data: null
							});
						})
					);
				}
				cb(null, true);
			}
		}).array('files', UploadHelper.maxFile);

		upload(req, res, function (err: any) {
			if (req.files) {
				if (err instanceof multer.MulterError) {
					if (err.code === 'LIMIT_FILE_SIZE') {
						err.message = 'Fichier trop volumineux';
					} else if (err.code === 'LIMIT_FILE_COUNT') {
						err.message = 'Nombre maximum de fichier atteint';
					}
					return coddyger.api(
						res,
						new Promise((resolve) => {
							resolve({
								status: 422,
								message: err.message,
								data: null
							});
						})
					);
				} else if (req.fileValidationError) {
					return coddyger.api(
						res,
						new Promise((resolve) => {
							resolve({
								status: defines.status.clientError,
								message: 'Type de fichier non autorisé',
								data: null
							});
						})
					);
				} else if (err) {
					return coddyger.api(
						res,
						new Promise((resolve) => {
							resolve({
								status: defines.status.serverError,
								message: defines.message.tryCatch,
								data: null
							});
						})
					);
				}
			} else {
				req.files = [];
			}

			next();
		});
	}

	static save(file: any) {
		return new Promise((resolve, reject) => {
			const tempPath = file.path;
			let fileExt = UploadHelper.buildExt(file.mimetype);
			let fullFilePath = UploadHelper.uploadPath + file.filename + '.' + fileExt;
			let filename: string = file.filename + '.' + fileExt;

			fs.rename(tempPath, fullFilePath, (err) => {
				if (err) reject(err);

				if (coddyger.file.exists(tempPath)) {
					fs.rmdirSync(tempPath);
				}
				resolve({ filename, fullFilePath });
			});
		}).catch((error) => {
			return { error: true, data: error };
		});
	}
	static buildExt(mimetype: string) {
		return mime.extension(mimetype);
	}
	static async buildUploadPath() {
		// CREATE TEMPORY UPLOAD PATH
		if (!fs.existsSync(this.uploadPathTmp)) {
			await fs.promises.mkdir(this.uploadPathTmp, { recursive: true });
		}
		// CREATE UPLOAD PATH
		if (!fs.existsSync(this.uploadPath)) {
			await fs.promises.mkdir(this.uploadPath, { recursive: true });
		}
	}
}
