import coddyger ,{ defines, env, IData, IErrorObject } from 'coddyger';
import { TokenHelper } from './token.helper';
import { IUser, UserSet } from '../modules/user';
import { IProfile, ProfileSet } from '../modules/profile';
import { locale } from '../public';

const dao:IData<IUser> = new UserSet
const daoProfile:IData<IProfile> = new ProfileSet

const profiles:IProfile[] = [
	{
		_id: coddyger.string.generateObjectId(),
		slug: "default",
		title: 'Master',
		code: 'master',
		ability: [
			{
				subject: 'all',
				action: 'manage'
			}
		],
		status: 'active',
		user: null,
	},
	{
		_id: coddyger.string.generateObjectId(),
		slug: "client",
		title: 'Client',
		code: 'client',
		ability: [
			{
				subject: 'article',
				action: 'manage'
			},
			{
				subject: 'dashboard',
				action: 'read'
			}
		],
		status: 'active',
		user: null,
	}
]

export class UserHelper {
	static generateToken(apikey: string, controllerLabel: string) {
		return new Promise(async (resolve, reject) => {
			let key = env.jwt.public;

			if (key !== apikey) {
				resolve({ status: defines.status.clientError, message: locale.user.controller.apiKeyNotFound, data: null });
			} else {
				let token = TokenHelper.generateAuth({
					data: apikey,
					reg: new Date()
				});

				resolve({ status: defines.status.requestOK, message: 'ok', data: token });
			}
		}).catch((err: any) => {
			return coddyger.catchReturn(err, controllerLabel, 'generateToken');
		});
	}

	static verifyToken(payload:any, controllerLabel: string) {
		return new Promise(async (resolve, reject) => {
			delete payload.exp;
      delete payload.iat;

			const token:any = await TokenHelper.generate(payload)

			if (token) {
        resolve({ status: defines.status.requestOK, message: 'ok', data: token });
      } else {
        reject(token)
      }
		}).catch((err: any) => {
			return coddyger.catchReturn(err, controllerLabel, 'verifyToken');
		});
	}

	static async buildDefaultAccount() {
		try {
			await UserHelper.buildDefaultProfile();

			const count: any = await dao.select({});

			if (count.totalRows <= 0) {
				const profile: any = await daoProfile.selectOne({ code: 'master' });

				const data: IUser = {
					_id: coddyger.string.generateObjectId(),
					password: await coddyger.string.encryptPassword('demo'),
					email: process.env.DEFAULT_ACCOUNT,
					firstname: 'John',
					lastname: 'Doe',
					role: 'admin',
					status: 'active',
					profile: profile._id,
					user: null
				};

				const save: any | IErrorObject = await dao.save(data);

				if (save.error) {
					// If there is an error saving to the local database, reject with the error response
					throw data;
				} else {
					// Resolve with a success response and the updated item
					coddyger.konsole('Compte par défaut ajouté: ' + process.env.DEFAULT_ACCOUNT);
				}
			}
		} catch (err: any) {
			coddyger.konsole('La création de compte par défaut a échoué');
			return { error: true, data: err };
		}
	}

	static isUsernameValid(username: string): boolean {
		// Check if the username length is between 3 and 20 characters
		if (username.length < 3 || username.length > 20) {
			return false;
		}

		// Check if the username contains only alphanumeric characters
		const alphanumericRegex = /^[a-zA-Z0-9]+$/;
		if (!alphanumericRegex.test(username)) {
			return false;
		}

		// If both conditions are met, the username is valid
		return true;
	}

	static async selectCount() {
		try {
			// Récupère tous les éléments
			const result: any = await dao.select({});

			// Initialise un objet pour stocker les comptes
			const counts: any = {
				tous: 0,
				active: 0,
				pending: 0,
				removed: 0,
			};

			// Boucle sur tous les éléments dans result
			for (const item of result) {
				// Supprime le mot de passe de l'élément
				const { password, status, ref, locked } = item;

				// Incrémente le compteur "tous"
				counts.tous++;

				// Incrémente le compteur correspondant à l'état du compte
				switch (status) {
					case "active":
						counts.active++;
						break;
					case "pending":
						counts.pending++;
						break;
					case "removed":
						counts.removed++;
						break;
				}
			}

			// Retourne les comptes
			return counts;
		} catch (error) {
			// Retourne une erreur si une exception est levée
			return { error: true, data: error };
		}
	}

	static getAbilitiesKey(ability : any) {
		let abilityKeyArray: any = [];
		for (let x in ability) {
				let a: any = ability[x];
				abilityKeyArray.push(a)
		}

		return abilityKeyArray
	}

	static async isData(payloads : any, set: any) : Promise<boolean | null> {
		try {
			let res: boolean = false;

			for (let payload of payloads) {
				if (!coddyger.string.isValidObjectId(payload)) {
					res = false
				} else {
					if (!coddyger.string.isEmpty(payload)) {
						const doc: any = await set.exist({ _id: payload });
						if (doc.error) {
							throw doc.error;
						} else {
							res = doc; 
						}
					} else {
						res = false;
					}
				}
			}
			return res;
		} catch (error:any) {
			return null
		}
	}

	private static async buildDefaultProfile() {
		try {
			for(let profile of profiles) {
				const isProfile: any = await daoProfile.exist({ slug: profile.slug });
				if (isProfile === false) {
					console.log(`Ajout du profile: ${profile.title}`)
					await daoProfile.save(profile);
					console.log('Terminé...') 
				}
			}
		} catch (err: any) {
			return { error: true, data: err };
		}
	}

	static async normaleProfileUserId() {
		const users:any = await dao.selectHug()
		let tmp:Array<any> = [];

		let succeed:number = 0;
		let errored:number = 0;

		for(let user of users) {
			if(coddyger.string.isValidObjectId(user.user) === false) {
				if(user.user == "Compte généré automatiquement") {
					user.user = null
				} else {
					user.user = coddyger.string.toObjectId(user.user)
				}
			}

			if(coddyger.string.isValidObjectId(user.profile) === false) {
				if(user.profile !== null) {
					user.profile = coddyger.string.toObjectId(user.profile)
				}
			}

			tmp.push(user)
		}

		for(let t of tmp) {
			const _id = t._id
			delete t._id
			const update:any = await dao.update({_id}, t)

			if(update.error) {
				errored++
			} else {
				succeed++
			}
		}

		console.log(succeed + ' items updated')
		console.log(errored + ' items errored')
	}

	static async normaleProfile() {
		const items:any = await daoProfile.selectHug()
		let tmp:Array<any> = [];

		let succeed:number = 0;
		let errored:number = 0;

		for(let item of items) {
			if(coddyger.string.isValidObjectId(item.user) === false) {
				if(item.user == "") {
					item.user = null
				} else {
					item.user = coddyger.string.toObjectId(item.user)
				}
			}

			tmp.push(item)
		}

		for(let t of tmp) {
			const _id = t._id
			delete t._id
			const update:any = await daoProfile.update({_id}, t)

			if(update.error) {
				errored++
			} else {
				succeed++
			}
		}

		console.log(succeed + ' profiles updated')
		console.log(errored + ' profiles errored')
	}

	static async normaleAccount() {
		const items:any = await dao.selectHug()

		let succeed:number = 0;
		let errored:number = 0;

		for(let t of items) {
			const _id = t._id
			delete t._id
			const update:any = await dao.update({_id}, t)

			if(update.error) {
				errored++
			} else {
				succeed++
			}
		}

		console.log(succeed + ' account updated') 
		console.log(errored + ' account errored')
	}

	static isAzureAccount(account: string): boolean {
		const domains: string[] = ['gs2e.ci', 'cie.ci', 'sodeci.ci']

		const domain:string = account.split('@')[1]

		if(coddyger.inArray(domain, domains)) {
			return true
		} else {
			return false
		}
	}
}
