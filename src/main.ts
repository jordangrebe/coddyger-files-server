import express from 'express';
import http from 'http';
import coddyger, { env, getDatabaseAccess } from 'coddyger';

import './routes';
import { SocketIOHelper, UserHelper } from './helpers';
import RouterService from './router';
import { CategoryService } from './modules/category';

class Main {
	public app: express.Application;
	public server: http.Server; // Create a server instance
	public router: any;
	public socket: any;

	constructor() {
		this.app = express();
		this.server = http.createServer(this.app); // Create an HTTP server

		const options = {
			cors: {
				origin: '*'
			}
		};
		const io = require('socket.io')(this.server, options);
		this.socket = new SocketIOHelper(io);

		RouterService.run(this.app);
		this.listen();
		this.socket.run();
	}

	private listen(): void {
		// Default port set
		const port = env.server.port;

		this.server
			.listen(port, async () => {
				coddyger.konsole('Serveur connecté! :: ' + port);

				if (env.database.useDatabase === 'yes') {
					const databaseAccess = getDatabaseAccess();
					await databaseAccess.connect();
				}
				
				UserHelper.buildDefaultAccount()
				CategoryService.buildDefaults()
			})
			.on('error', (err: any) => {
				console.log('LISTEN:ERROR', err.code);
				if (err.code === 'EADDRINUSE') {
					console.log(`Port ${port} est déjà utilisé`, 1);
				} else {
					console.log(err, 1);
				}
			});
	}
}

export default new Main().app;
