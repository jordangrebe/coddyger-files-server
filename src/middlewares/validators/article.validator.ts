import {check} from "express-validator";
import { locale } from "../../public";

export class articleValidator {
    static saveStepOne() {
        return [
            check('title').not().isEmpty().withMessage(locale.article.router.titleRequired),
        ]
    }

    static update() {
        return [
            check('_id').not().isEmpty().withMessage(locale.router.idRequired),
            check('title').not().isEmpty().withMessage(locale.article.router.titleRequired),
        ]
    }
}
