import {check} from "express-validator";
import { locale } from "../../public";

export class categoryValidator {
    static save() {
        return [
            check('title').not().isEmpty().withMessage(locale.category.router.titleRequired),
        ]
    }

    static update() {
        return [
            check('_id').not().isEmpty().withMessage(locale.router.idRequired),
            check('title').not().isEmpty().withMessage(locale.category.router.titleRequired),
        ]
    }
}
