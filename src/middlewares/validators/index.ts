export * from './main.validator';
export * from './user.validator';
export * from './profile.validator';
export * from './website.validator';
export * from './category.validator';
export * from './article.validator';

import { validationResult } from 'express-validator';
import coddyger, { defines } from 'coddyger';

export default class Validator {
	static validate(req: any, res: any, next: any) {
		const errors: any = validationResult(req);
		if (!errors.isEmpty()) {
			return coddyger.api(
				res,
				new Promise((resolve) => {
					resolve({
						status: defines.status.clientError,
						message: errors.errors[0].msg,
						data: errors.errors
					});
				})
			);
		} else {
			next();
		}
	}
}
