import {check} from "express-validator";
import { locale } from "../../public";

export class profileValidator {
    static save() {
        return [
            check('title').not().isEmpty().withMessage(locale.profile.router.titleRequired),
            check('ability').not().isEmpty().withMessage(locale.profile.router.abilityRequired),
        ]
    }

    static update() {
        return [
            check('_id').not().isEmpty().withMessage(locale.router.idRequired),
            check('title').not().isEmpty().withMessage(locale.profile.router.titleRequired),
            check('ability').not().isEmpty().withMessage(locale.profile.router.abilityRequired),
        ]
    }
}
