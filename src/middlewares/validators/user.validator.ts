import { check } from 'express-validator';
import { locale } from '../../public';

export class userValidator {
	static accessToken() {
		return [check('apikey').not().isEmpty().withMessage(locale.user.router.apikeyRequired)];
	}

	static login() {
		return [
			check('login').not().isEmpty().withMessage(locale.user.router.emailRequired),
			check('password').not().isEmpty().withMessage(locale.user.router.passwordRequired)
		];
	}

	static register() {
		return [
			check('username').not().isEmpty().withMessage(locale.user.router.usernameRequired),
			check('email').not().isEmpty().withMessage(locale.user.router.emailRequired),
			check('password').not().isEmpty().withMessage(locale.user.router.passwordRequired),
			check('passwordConfirmation').not().isEmpty().withMessage(locale.user.router.passwordConfirmationRequired)
		];
	}

	static setPassword() {
		return [
			check('user').not().isEmpty().withMessage(locale.user.router.idRequired),
			check('password').not().isEmpty().withMessage(locale.user.router.passwordRequired),
			check('passwordConfirmation').not().isEmpty().withMessage(locale.user.router.passwordConfirmationRequired)
		];
	}

	static save() {
		return [
			check('email').not().isEmpty().withMessage(locale.user.router.emailRequired),
			check('roles').not().isEmpty().withMessage(locale.user.router.rolesRequired)
		];
	}

	static edit() {
		return [
			check('concern').not().isEmpty().withMessage(locale.router.idRequired),
			check('profile').not().isEmpty().withMessage(locale.user.router.profileRequired)
		];
	}

	static editStatus() {
		return [
			check('email').not().isEmpty().withMessage(locale.user.router.emailRequired),
			check('status').not().isEmpty().withMessage(locale.router.statusRequired)
		];
	}

	static setDefaultProfile() {
		return [
			check('email').not().isEmpty().withMessage(locale.user.router.emailRequired),
			check('profile').not().isEmpty().withMessage(locale.user.router.profileRequired)
		];
	}

	static pushProfile() {
		return [
			check('email').not().isEmpty().withMessage(locale.user.router.emailRequired),
			check('profiles').not().isEmpty().withMessage(locale.user.router.profileRequired)
		];
	}
}
