import {check} from "express-validator";
import { locale } from "../../public";

export class websiteValidator {
    static save() {
        return [
            check('name').not().isEmpty().withMessage(locale.website.router.titleRequired),
        ]
    }

    static update() {
        return [
            check('_id').not().isEmpty().withMessage(locale.router.idRequired),
            check('name').not().isEmpty().withMessage(locale.website.router.titleRequired),
        ]
    }
}
