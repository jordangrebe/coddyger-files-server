import coddyger, { IData, IErrorObject, defines } from 'coddyger'
import { locale } from "../../public";
import { IArticle, ArticleSet, ArticleService } from "./";
import { IUser, UserSet } from '../user';
import { UploadHelper } from '../../helpers';
import { ActivitySet, IActivity } from '../../commons/models';
import { CategorySet, ICategory } from '../category';
import FileHelper from '../../helpers/file.helper';

const controllerLabel: string = 'ArticleController';

export class ArticleController {
	private readonly dao: IData<IArticle>;
	private readonly daoCategory: IData<ICategory>;
	private readonly daoActivity: IData<IActivity>;
	private readonly daoUser: IData<IUser>;

	constructor() {
		this.dao = new ArticleSet();
		this.daoCategory = new CategorySet();
		this.daoActivity = new ActivitySet();
		this.daoUser = new UserSet();
	}

	// Function to save Article as draft with title
	saveStepOne(item: IArticle) {
		return new Promise(async (resolve, reject) => {
			// Extracting necessary information from the item
			let title: any = item.title;
			let user: any = item.user;

			if (!coddyger.string.isValidObjectId(user)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId("du compte"), data: null });
			} else {
				const isUser: any = await this.daoUser.exist({ _id:user });
				const isTitle: any = await this.dao.exist({ title, user });

				// Validation checks for the item name
				if (isUser === false) {
					resolve({ status: defines.status.authError, message: locale.controller.notAdminAccount, data: null });
				} else if (isTitle) {
					resolve({ status: defines.status.clientError, message: locale.exist('ce titre'), data: null });
				} else {
					const theLast:any = await this.dao.selectLatest();
					
					// Generate a new ObjectId for the item
					item._id = coddyger.string.generateObjectId();
					item.slug = coddyger.buildSlug('POST', theLast ? theLast.slug : null);
					item.url = ArticleService.buildUrl(item.title)

					// Save the item to the local database
					const data: any | IErrorObject = await this.dao.save(item);

					if (data.error) {
						// If there is an error saving to the local database, reject with the error response
						reject(data);
					} else {
						const itemModelName = item.constructor.name;

						await this.daoActivity.save({
							_id: coddyger.string.generateObjectId(),
              user,
              item: item._id,
							itemType: itemModelName
						})
						// Retrieve the saved item from the database
						const updatedItem: any = await this.dao.selectOne({ _id: item._id });

						// Resolve with a success response and the updated item
						resolve({
							status: defines.status.requestOK,
							message: locale.controller.draftDone,
							data: updatedItem
						});
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'saveStepOne');
		});
	}

	joinImage(item: IArticle) {
		return new Promise(async (resolve, reject) => {
			let _id: string = item._id!;
			let image: any = item.image;

			if (!coddyger.string.isValidObjectId(_id)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId("de l'enregistrement"), data: null });
			} else if (coddyger.string.isEmpty(image)) {
				resolve({ status: defines.status.clientError, message: locale.required("le fichier image"), data: null });
			} else {
				// Controller l'existence de l'élément
				let isData: any = await this.dao.exist({ _id });

				if (isData.error) {
					reject(isData);
				} else if (isData === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Enregistrement'), data: null });
				} else {
					const fileSave:any = await FileHelper.save(image)

					if (fileSave.error) {
						reject(fileSave.data)
					} else {
						const filename:string = fileSave.filename
						item.image = filename
						item.status = 'active'
						item.publishedAt = new Date()
						// Update the item with the history entry
						const update: any = await this.dao.update({ _id }, item);
						if (update.error) {
							reject(update);
						} else {
							// Retrieve the updated item from the database
							const updatedItem: any = await this.dao.selectOne({ _id }, '-__v');
							// Resolve with a success response and the updated item
							resolve({
								status: defines.status.requestOK,
								message: locale.controller.done,
								data: updatedItem
							});
						}
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'joinImage');
		});
	}

	joinAudio(item: IArticle) {
		return new Promise(async (resolve, reject) => {
			let _id: string = item._id!;
			let audio: any = item.audio;

			if (!coddyger.string.isValidObjectId(_id)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId("de l'enregistrement"), data: null });
			} else if (coddyger.string.isEmpty(audio)) {
				resolve({ status: defines.status.clientError, message: locale.required("le fichier audio"), data: null });
			} else {
				// Controller l'existence de l'élément
				let isData: any = await this.dao.exist({ _id });

				if (isData.error) {
					reject(isData);
				} else if (isData === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Enregistrement'), data: null });
				} else {
					const fileSave:any = await FileHelper.save(audio)

					if (fileSave.error) {
						reject(fileSave.data)
					} else {
						const filename:string = fileSave.filename
						
						item.audio = filename
						item.status = 'active'
						item.publishedAt = new Date()
						// Update the item with the history entry
						const update: any = await this.dao.update({ _id }, item);
						if (update.error) {
							reject(update);
						} else {
							// Retrieve the updated item from the database
							const updatedItem: any = await this.dao.selectOne({ _id }, '-__v');
							// Resolve with a success response and the updated item
							resolve({
								status: defines.status.requestOK,
								message: locale.controller.done,
								data: updatedItem
							});
						}
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'joinAudio');
		});
	}

	uploadFile(item: any) {
		return new Promise(async (resolve, reject) => {
			let file: any = item.file;

			if (coddyger.string.isEmpty(file)) {
				resolve({ status: defines.status.clientError, message: locale.required("le fichier"), data: null });
			} else {
				// Enregistrer le fichier avec un personnalisé
				const fileSave:any = await FileHelper.save(file)

				if (fileSave.error) {
					reject(fileSave.data)
				} else {
					const filename:string = fileSave.filename
					resolve({
						status: defines.status.requestOK,
						message: locale.controller.done,
						data: `${filename}`
					});
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'uploadImage');
		});
	}	

		// Function to save Article
	save(item: IArticle) {
		return new Promise(async (resolve, reject) => {
			// Extracting necessary information from the item
			let title: any = item.title;
			let user: any = item.user;

			if (!coddyger.string.isValidObjectId(user)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId("du compte"), data: null });
			} else {
				const isUser: any = await this.dao.exist({ title });
				const isTitle: any = await this.dao.exist({ title });

				// Validation checks for the item name
				if (isUser === false) {
					resolve({ status: defines.status.clientError, message: locale.controller.notAdminAccount, data: null });
				} else if (isTitle) {
					resolve({ status: defines.status.clientError, message: locale.exist('ce titre'), data: null });
				} else {
					const theLast:any = await this.dao.selectLatest();
					// Generate a new ObjectId for the item
					item._id = coddyger.string.generateObjectId();
					item.slug = coddyger.buildSlug('POST', theLast ? theLast.slug : null);

					// Save the item to the local database
					const data: any | IErrorObject = await this.dao.save(item);

					if (data.error) {
						// If there is an error saving to the local database, reject with the error response
						reject(data);
					} else {
						// Retrieve the saved item from the database
						const updatedItem: any = await this.dao.selectOne({ _id: item._id });

						// Resolve with a success response and the updated item
						resolve({
							status: defines.status.requestOK,
							message: locale.controller.successSave,
							data: updatedItem
						});
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'save');
		});
	}

	// Function to update Article
	update(item: IArticle) {
		return new Promise(async (resolve, reject) => {
			let _id: string = item._id!;
			let title: any = item.title;
			let categories: Array<any> = item.category;

			if (!coddyger.string.isValidObjectId(_id)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId("de l'enregistrement"), data: null });
			} else {
				let isCategory: boolean = false;
				// Controller l'existence de l'élément
				let isData: any = await this.dao.exist({ _id });
				console.log(categories.length)

				if(categories.length >= 1) {
					for(const category of categories) {
						if (!coddyger.string.isValidObjectId(category)) {
							resolve({ status: defines.status.clientError, message: locale.wrongObjectId("de la catégorie"), data: null });
							break
						} else {
							const categoryExist: any = await this.daoCategory.exist({ _id: category });
							if(categoryExist === true) {
								isCategory = categoryExist
							} else {
								isCategory = false;
							}
						}
					}
				}

				if (isData === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Enregistrement'), data: null });
				} else if (categories.length >= 1 && isCategory === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Catégory'), data: null });
				} else {
					let isTitle: any = await this.dao.exist({ title });
					let ownTitle: any = await this.dao.exist({ $and: [{ title }, { _id }] });

					if (isTitle && !ownTitle) {
						resolve({ status: defines.status.clientError, message: locale.notfound('Enregistrement'), data: null });
					} else {
						// Update the item with the history entry
						const update: any = await this.dao.update({ _id }, item);
						if (update.error) {
							reject(update);
						} else {
							// Retrieve the updated item from the database
							const updatedItem: any = await this.dao.selectOne({ _id }, '-__v');
							// Resolve with a success response and the updated item
							resolve({
								status: defines.status.requestOK,
								message: locale.controller.done,
								data: updatedItem
							});
						}
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'update');
		});
	}

	updateStatus(item: {_id: string, status: string, user: string}) {
		return new Promise(async (resolve, reject) => {
			const _id:string = item._id
			const status:string = item.status
			const user:string = item.user
			
			if (!coddyger.string.isValidObjectId(_id)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId("de l'enregistrement"), data: null });
			} else if (!coddyger.string.isValidObjectId(user)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId("du compte"), data: null });
			} else {
				// Controller l'existence de l'élément
				let isData: any = await this.dao.exist({ _id });
				let isUser:IErrorObject | boolean = await this.daoUser.exist({_id: user});

				if(!isUser) {
					resolve({ status: defines.status.clientError, message: locale.controller.notAdminAccount, data: null });
				} else if (isData === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Enregistrement'), data: null });
				} else {
					// Update the item with the history entry
					const update: any = await this.dao.update({ _id }, {status});
					if (update.error) {
						reject(update);
					} else {
						// Retrieve the updated item from the database
						const updatedItem: any = await this.dao.selectOne({ _id }, '-__v');
						// Resolve with a success response and the updated item
						resolve({
							status: defines.status.requestOK,
							message: locale.controller.done,
							data: updatedItem
						});
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'updateStatus');
		});
	}

	// Function to remove Article
	remove(_id: string) {
		return new Promise(async (resolve, reject) => {
			if (!coddyger.string.isValidObjectId(_id)) {
				resolve({ status: defines.status.clientError, message: locale.controller.wrongObjectId, data: null });
			} else {
				// Controller l'existence de l'élément
				let isData: any = await this.dao.exist({ _id });

				if (isData.error) {
					reject(isData);
				} else if (isData === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Enregistrement'), data: null });
				} else {
					const remove: any = await this.dao.update({ _id }, { status: 'removed' });

					if (remove.error) {
						reject(remove);
					} else {
						// Retrieve the updated demand item from the database
						const updatedItem: any = await this.dao.selectOne({ _id });
						// Resolve with a success response and the updated demand item
						resolve({
							status: defines.status.requestOK,
							message: locale.controller.done,
							data: updatedItem
						});
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'remove');
		});
	}

	// Function to restore Article
	restore(_id: string) {
		return new Promise(async (resolve, reject) => {
			if (!coddyger.string.isValidObjectId(_id)) {
				resolve({ status: defines.status.clientError, message: locale.controller.wrongObjectId, data: null });
			} else {
				// Controller l'existence de l'élément
				let isData: any = await this.dao.exist({ _id });

				if (isData.error) {
					reject(isData);
				} else if (isData === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Enregistrement'), data: null });
				} else {
					const remove: any = await this.dao.update({ _id }, { status: 'active' });

					if (remove.error) {
						reject(remove);
					} else {
						// Retrieve the updated demand item from the database
						const updatedItem: any = await this.dao.selectOne({ _id });
						// Resolve with a success response and the updated demand item
						resolve({
							status: defines.status.requestOK,
							message: locale.controller.done,
							data: updatedItem
						});
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'remove');
		});
	}

	// Function to select with parameters Article
	select(payloads: { page?: number; pageSize?: number; query?: string; status?: string, user?: string, module?:string }) {
		return new Promise(async (resolve, reject) => {
			let page: number = payloads.page || 1;
			let pageSize: number = payloads.pageSize;
			let query: string = payloads.query!;
			let status: any = payloads.status!;
			let user: string = payloads.user!;

			let data: any | IErrorObject = {};

			if (coddyger.string.isEmpty(query) && coddyger.string.isEmpty(status)) {
				data = await this.dao.select({ params: { user }, page, pageSize });
			} else if (!coddyger.string.isEmpty(status)) {
				data = await this.dao.select({ params: { status, user}, page, pageSize });
			} else {
				data = await this.dao.select({
					params: {
						$and: [
							{
								$or: [
									{ slug: { $regex: query || '', $options: 'i' } },
									{ title: { $regex: query || '', $options: 'i' } },
									{ usrl: { $regex: query || '', $options: 'i' } },
								]
							}, { user }
						]
					},
					page,
					pageSize
				});
			}

			if (data.error) {
				reject(data);
				return;
			}

			let rowsNew: IArticle[] = [];
			let rows: IArticle[] = data.rows;
			delete data.rows;

			if(coddyger.string.isEmpty(payloads.module) === false) {
				for(const row of rows) {
					const categories:any[] = row.category
					for(const category of categories) {
						if(category.module === payloads.module) {
							rowsNew.push(row)
						}
					}
				}
			} else {
				rowsNew = rows
			}

			resolve({
				status: defines.status.requestOK,
				message: data,
				data: rowsNew
			});
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'select');
		});
	}

	// Function to select Article by parameters
	selectByParams(params: any, page?: number) {
		return new Promise(async (resolve, reject) => {
			const data: any | IErrorObject = await this.dao.select({ params, page });
			if (data.error) {
				reject(data);
				return;
			}

			const rows: IArticle[] = data.rows;
			delete data.rows;

			resolve({
				status: defines.status.requestOK,
				message: data,
				data: rows
			});
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'selectByParams');
		});
	}

	// Function to select all Article
	selectAll(user?:string) {
		return new Promise(async (resolve, reject) => {
			const data: any | IErrorObject = await this.dao.selectHug({user});
			if (data.error) {
				reject(data);
				return;
			}

			const rows: ICategory[] = data;

			resolve({
				status: defines.status.requestOK,
				message: {
					totalRows: rows.length
				},
				data: rows || []
			});
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'selectAll');
		});
	}

	// Function to select detail of Article by id
	selectOne(payload: string) {
		return new Promise(async (resolve, reject) => {
			if (!coddyger.string.isValidObjectId(payload)) {
				resolve({ status: defines.status.clientError, message: locale.controller.wrongObjectId, data: null });
			} else {
				const local: any | IErrorObject = await this.dao.selectOne({ _id: payload });

				if (local) {
					resolve({
						status: defines.status.requestOK,
						message: 'OK',
						data: local
					});
				} else {
					resolve({
						status: defines.status.notFound,
						message: locale.notfound('Enregistrement'),
						data: null
					});
					return;
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'selectOne');
		});
	}

	// Function to search Article
	search(payload?: string, page?: number) {
		return new Promise(async (resolve, reject) => {
			const params = {
				$or: [
					{ slug: { $regex: new RegExp(payload, 'i') } },
					{ title: { $regex: new RegExp(payload, 'i') } },
					{ status: { $regex: new RegExp(payload, 'i') } }
				]
			};

			const data: any | IErrorObject = await this.dao.select({ params, page });
			if (data.error) {
				reject(data);
				return;
			}

			const rows: IArticle[] = data.rows;
			delete data.rows;

			resolve({
				status: defines.status.requestOK,
				message: data,
				data: rows
			});
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'search');
		});
	}
}
