import mongoose, { Schema, Document } from 'mongoose';
import coddyger, { IData, IErrorObject, defines, LoggerService, LogLevel, MongoDbDao } from 'coddyger'

export interface IArticle {
	_id?: string; // MongoDB generated id
	slug?: string; // Reference of the document
	title?: string; // Title of the document
	url?: string; // URL of the document
	content?: string; // Content of the document
	resume?: string; // Content of the document
	comments: Array<any>;
	ratings: Array<any>;
	status?: string; // Status of the document - active - removed - archived
	publishedAt: Date;
	image: string;
	audio: string;
	eventDate: string;
	eventHour: string;
	eventAddress: string;
	category?: Array<any>;
	user?: any; // The id of the user that owns or created the document
}

const schema = new mongoose.Schema<IArticle>(
	{
		_id: Schema.Types.ObjectId,
		slug: String,
		title: String,
		url: String,
		content: String,
		resume: String,
		publishedAt: { type: Date, default: null },
		image: String,
		audio: String,
		eventDate: String,
		eventHour: String,
		eventAddress: String,
		ratings: [{ type: Number, min: 1, max: 5 }],
		status: { type: String, enum: ['active', 'archived', 'removed', 'draft'], default: 'draft' },
		category: [{ type: Schema.Types.ObjectId, ref: 'Category' }],
		user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
	},
	{ timestamps: true }
);

const model = mongoose.model<IArticle>('Article', schema);

export class ArticleSet extends MongoDbDao<Document> implements IData<Document> {
	defaultModel = model;

	constructor() {
		super();
	}

	props: string = 'slug title';
	userProps: string = 'slug email lastname firstname';
	setTitle: string = 'ArticleSet';

	select(payloads: { 
		params?: any, 
		excludes?: string, 
		page?: number, 
		pageSize?: number,
		sort?: string,
		orderBy?: string; 
	}): Promise<any> {
		return new Promise(async (resolve, reject) => {
			if (coddyger.string.isEmpty(payloads.params.status)) {
				payloads.params = { ...payloads.params, status: { $nin: ['removed', 'archived'] } };
			}

			let page: number = Number(payloads.page) || 1;
			const pageSize: number = Number(payloads.pageSize) || 10;

			page = page === 0 ? 1 : page;

			const startIndex = (page - 1) * pageSize;
			const model = this.defaultModel;
			let sortBy: string = payloads.sort ?? 'createdAt';
			let orderBy: string = payloads.orderBy ?? 'desc';

			// Create sort object
			let sortObject: any = {};
			if (sortBy) {
				sortObject = {
					[sortBy]: orderBy === 'desc' ? -1 : 1
				};
			} else {
				// Default sort by createdAt in descending order if no sortBy is provided
				sortObject = { createdAt: -1 };
			}

			const [rows, totalRows] = await Promise.all([
				model
					.find(payloads.params, "-password -__v")
					.sort(sortObject)
					.skip(startIndex)
					.limit(pageSize)
					.lean(),
				model.countDocuments(payloads.params)
			]);

			if (rows) {
				const totalPages = Math.ceil(totalRows / pageSize);
				const countRowsPerPage = rows.length;
				const totalPagesPerQuery = Math.ceil(totalRows / pageSize);

				resolve({
					rows,
					totalRows,
					totalPages,
					countRowsPerPage,
					totalPagesPerQuery
				});
			} else {
				reject({ rows, totalRows });
			}
		}).catch((e: any) => {
			LoggerService.log({
				type: LogLevel.Error,
				content: JSON.stringify(e),
				location: this.setTitle,
				method: 'select'
			});
			return { error: true, data: e, message: defines.message.tryCatch };
		});
	}

	selectFrontend(payloads: { params?: any; excludes?: string; sort?: object; page?: number; pageSize?: number }): Promise<any> {
		return new Promise(async (resolve, reject) => {
			if (coddyger.string.isEmpty(payloads.params.status)) {
				payloads.params = { ...payloads.params, status: { $nin: ['removed', 'archived'] } };
			}

			let page: number = Number(payloads.page) || 1;
			const pageSize: number = Number(payloads.pageSize) || 10;

			page = page === 0 ? 1 : page;

			const startIndex = (page - 1) * pageSize;
			const model = this.defaultModel;

			const [rows, totalRows] = await Promise.all([
				model
					.find(payloads.params, payloads.excludes)
					.sort({ createdAt: -1 })
					.skip(startIndex)
					.limit(pageSize)
					.lean()
					.populate({
						path: 'user',
						select: this.userProps,
						options: { sort: { createdAt: -1 } }
					}).populate('category'),
				model.countDocuments(payloads.params)
			]);

			if (rows) {
				const totalPages = Math.ceil(totalRows / pageSize);
				const countRowsPerPage = rows.length;
				const totalPagesPerQuery = Math.ceil(totalRows / pageSize);

				resolve({
					rows,
					totalRows,
					totalPages,
					countRowsPerPage,
					totalPagesPerQuery
				});
			} else {
				reject({ rows, totalRows });
			}
		}).catch((e: any) => {
			LoggerService.log({
				type: LogLevel.Error,
				content: JSON.stringify(e),
				location: this.setTitle,
				method: 'select'
			});
			return { error: true, data: e, message: defines.message.tryCatch };
		});
	}

	selectHug(): Promise<Array<Document> | any> {
		return new Promise(async (resolve, reject) => {
			let doc = await this.defaultModel
				.find()
				.lean()
				.populate({
					path: 'user',
					select: this.userProps,
					options: { sort: { createdAt: -1 } }
				}).populate('category');

			if (!doc) {
				reject(doc);
			} else {
				resolve(doc);
			}
		}).catch((e: any) => {
			LoggerService.log({ type: LogLevel.Error, content: e, location: this.setTitle, method: 'selectHug' });
			return { error: true, data: e, message: defines.message.tryCatch };
		});
	}

	selectOne(params: object, fields?: string): Promise<Document | any> {
		return new Promise(async (resolve, reject) => {
			resolve(
				await this.defaultModel
					.findOne(params, fields)
					.lean()
					.populate({
						path: 'user',
						select: this.userProps,
						options: { sort: { createdAt: -1 } }
					}).populate('category')
			);
		}).catch((e: any) => {
			LoggerService.log({ type: LogLevel.Error, content: e, location: this.setTitle, method: 'selectOne' });
			return { error: true, data: e, message: defines.message.tryCatch };
		});
	}
}