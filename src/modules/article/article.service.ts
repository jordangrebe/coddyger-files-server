import coddyger, { IData, IErrorObject } from 'coddyger'
import { IArticle, ArticleSet } from './';

const dao: IData<IArticle> = new ArticleSet();

export class ArticleService {
	static sampleMethod(apikey: string, controllerLabel: string) {
		return new Promise(async (resolve, reject) => {

		}).catch((err: any) => {
			return coddyger.catchReturn(err, 'ArticleService', 'sampleMethod');
		});
	}

	static buildUrl(title: string) {
		const url = title.substring(0, 50).toLowerCase().replace(/[^a-z0-9]+/g, '-').replace(/^-+|-+$/g, '');
  	return url;
	}
}
