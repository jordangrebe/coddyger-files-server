import coddyger, { Controller, IData, IErrorObject, defines } from 'coddyger'
import { locale } from "../../public";
import { ICategory, CategorySet, CategoryService } from "./";
import { IUser, UserSet } from '../user';

const controllerLabel: string = 'CategoryController';

export class CategoryController extends Controller {
	private readonly daoUser: IData<IUser>;

	constructor() {
		super(CategorySet);
		this.daoUser = new UserSet();
	}

	// Function to save Category
	save(item: ICategory) {
		return new Promise(async (resolve, reject) => {
			// Extracting necessary information from the item
			let title: any = item.title;
			let user: any = item.user;
			let parent: string = item.parent;

			if (!coddyger.string.isValidObjectId(user)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId('du compte'), data: null });
			} else if (!coddyger.string.isValidObjectId(parent)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId('du parent'), data: null });
			} else {
				parent = parent == '' ? null : parent;

				const isUser: boolean | IErrorObject = await this.daoUser.exist({ _id: user });
				const isParent: boolean | IErrorObject = await this.dao.exist({ _id: parent });
				const isTitle: boolean | IErrorObject = await this.dao.exist({ title, user });

				// Validation checks for the item name
				if (isUser === false) {
					resolve({ status: defines.status.authError, message: locale.controller.notAdminAccount, data: null });
				} else if (isTitle === true) {
					resolve({ status: defines.status.clientError, message: locale.exist('ce titre'), data: null });
				} else if (!coddyger.string.isEmpty(parent) && isParent === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Parent'), data: null });
				} else {
					const parentData:any = await this.dao.selectOne({_id: parent})
					// Retrieve the latest item from the database
					const theLast: any = await this.dao.selectLatest();
					// Generate a new ObjectId for the item
					item._id = coddyger.string.generateObjectId();
					item.slug = coddyger.buildSlug('CAT', theLast ? theLast.slug : null);
					item.module = parentData.module

					// Save the item to the local database
					const data: any | IErrorObject = await this.dao.save(item);

					if (data.error) {
						// If there is an error saving to the local database, reject with the error response
						reject(data);
					} else {
						// Retrieve the saved item from the database
						const updatedItem: any = await this.dao.selectOne({ _id: item._id });

						// Resolve with a success response and the updated item
						resolve({
							status: defines.status.requestOK,
							message: locale.controller.successSave,
							data: updatedItem
						});
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'save');
		});
	}

	// Function to update Category
	update(item: ICategory) {
		return new Promise(async (resolve, reject) => {
			let _id: string = item._id!;
			let title: any = item.title;
			let user: any = item.user;
			let parent: any = item.parent;

			if (!coddyger.string.isValidObjectId(_id)) {
				resolve({
					status: defines.status.clientError,
					message: locale.wrongObjectId("de l'enregistrement"),
					data: null
				});
			} else if (!coddyger.string.isValidObjectId(user)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId('du compte'), data: null });
			} else if (!coddyger.string.isEmpty(parent) && !coddyger.string.isValidObjectId(parent)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId('du parent'), data: null });
			} else if (!CategoryService.isParentChildConflict(_id, parent) && !coddyger.string.isEmpty(parent)) {
				resolve({
					status: defines.status.clientError,
					message: locale.category.controller.parentChildConflict,
					data: null
				});
			} else if (!coddyger.string.isEmpty(parent) && _id == parent) {
				resolve({
					status: defines.status.clientError,
					message: locale.category.controller.cantBeOwnParent,
					data: null
				});
			} else {
				const isUser: boolean | IErrorObject = await this.daoUser.exist({ _id: user });
				const isParent: boolean | IErrorObject = await this.dao.exist({ _id: parent, user });
				const isTitle: boolean | IErrorObject = await this.dao.exist({ title, user });

				// Validation checks for the item name
				if (isUser === true) {
					resolve({ status: defines.status.clientError, message: locale.controller.notAdminAccount, data: null });
				} else if (isTitle === true) {
					resolve({ status: defines.status.clientError, message: locale.exist('ce titre'), data: null });
				} else if (!coddyger.string.isEmpty(parent) && isParent === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Parent'), data: null });
				} else {
					// Controller l'existence de l'élément
					let isData: any = await this.dao.exist({ _id });

					if (isData.error) {
						reject(isData);
					} else if (isData === false) {
						resolve({ status: defines.status.clientError, message: locale.notfound('Enregistrement'), data: null });
					} else {
						let isTitle: any = await this.dao.exist({ title, user });
						let ownTitle: any = await this.dao.exist({ $and: [{ title }, { _id }, { user }] });

						if (isTitle && !ownTitle) {
							resolve({ status: defines.status.clientError, message: locale.notfound('Enregistrement'), data: null });
						} else {
							// Update the item with the history entry
							const update: any = await this.dao.update({ _id }, item);
							if (update.error) {
								reject(update);
							} else {
								// Retrieve the updated item from the database
								const updatedItem: any = await this.dao.selectOne({ _id }, '-__v');
								// Resolve with a success response and the updated item
								resolve({
									status: defines.status.requestOK,
									message: locale.controller.done,
									data: updatedItem
								});
							}
						}
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'update');
		});
	}

	// Function to remove Category
	remove(_id: string) {
		return new Promise(async (resolve, reject) => {
			if (!coddyger.string.isValidObjectId(_id)) {
				resolve({ status: defines.status.clientError, message: locale.controller.wrongObjectId, data: null });
			} else {
				// Controller l'existence de l'élément
				let isData: any = await this.dao.exist({ _id });

				if (isData.error) {
					reject(isData);
				} else if (isData === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Enregistrement'), data: null });
				} else {
					const remove: any = await this.dao.update({ _id }, { status: 'removed' });

					if (remove.error) {
						reject(remove);
					} else {
						// Retrieve the updated demand item from the database
						const updatedItem: any = await this.dao.selectOne({ _id });
						// Resolve with a success response and the updated demand item
						resolve({
							status: defines.status.requestOK,
							message: locale.controller.done,
							data: updatedItem
						});
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'remove');
		});
	}

	// Function to restore Category
	restore(_id: string) {
		return new Promise(async (resolve, reject) => {
			if (!coddyger.string.isValidObjectId(_id)) {
				resolve({ status: defines.status.clientError, message: locale.controller.wrongObjectId, data: null });
			} else {
				// Controller l'existence de l'élément
				let isData: any = await this.dao.exist({ _id });

				if (isData.error) {
					reject(isData);
				} else if (isData === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Enregistrement'), data: null });
				} else {
					const remove: any = await this.dao.update({ _id }, { status: 'active' });

					if (remove.error) {
						reject(remove);
					} else {
						// Retrieve the updated demand item from the database
						const updatedItem: any = await this.dao.selectOne({ _id });
						// Resolve with a success response and the updated demand item
						resolve({
							status: defines.status.requestOK,
							message: locale.controller.done,
							data: updatedItem
						});
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'remove');
		});
	}

	// Function to select with parameters Category
	select(payloads: { page?: number; pageSize?: number; query?: string; status?: string; user?: string }) {
		return new Promise(async (resolve, reject) => {
			let page: number = payloads.page || 1;
			let pageSize: number = payloads.pageSize;
			let query: string = payloads.query!;
			let status: any = payloads.status!;
			let user: any = payloads.user!;

			let data: any | IErrorObject = {};

			if (coddyger.string.isEmpty(query) && coddyger.string.isEmpty(status)) {
				data = await this.dao.select({ params: { user }, page, pageSize });
			} else if (!coddyger.string.isEmpty(status)) {
				data = await this.dao.select({ params: { status, user }, page, pageSize });
			} else {
				data = await this.dao.select({
					params: {
						$and: [
							{
								$or: [
									{ slug: { $regex: query || '', $options: 'i' } },
									{ title: { $regex: query || '', $options: 'i' } }
								]
							},
							{ user }
						]
					},
					page,
					pageSize
				});
			}

			if (data.error) {
				reject(data);
				return;
			}

			const rows: ICategory[] = data.rows;
			delete data.rows;

			resolve({
				status: defines.status.requestOK,
				message: data,
				data: rows
			});
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'select');
		});
	}

	// Function to select all Profile
	selectByParent(parent: string, user: string) {
		return new Promise(async (resolve, reject) => {
			const data: any | IErrorObject = await this.dao.selectHug({
				$or: [
					{ $and: [{ parent: null }, { user }] },
					{ $and: [{ module: parent }, { user }] }
				]
			});
			if (data.error) {
				reject(data);
				return;
			}

			const rows: ICategory[] = data;

			resolve({
				status: defines.status.requestOK,
				message: {
					totalRows: rows.length
				},
				data: rows || []
			});
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'selectAll');
		});
	}

	// Function to select Category by parameters
	selectByParams(params: any, page?: number) {
		return new Promise(async (resolve, reject) => {
			const data: any | IErrorObject = await this.dao.select({ params, page });
			if (data.error) {
				reject(data);
				return;
			}

			const rows: ICategory[] = data.rows;
			delete data.rows;

			resolve({
				status: defines.status.requestOK,
				message: data,
				data: rows
			});
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'selectByParams');
		});
	}
	// Function to select detail of Category by id
	selectOne(payload: string) {
		return new Promise(async (resolve, reject) => {
			if (!coddyger.string.isValidObjectId(payload)) {
				resolve({ status: defines.status.clientError, message: locale.controller.wrongObjectId, data: null });
			} else {
				const local: any | IErrorObject = await this.dao.selectOne({ _id: payload });

				if (local) {
					resolve({
						status: defines.status.requestOK,
						message: 'OK',
						data: local
					});
				} else {
					resolve({
						status: defines.status.notFound,
						message: locale.notfound('Enregistrement'),
						data: null
					});
					return;
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'selectOne');
		});
	}
}
