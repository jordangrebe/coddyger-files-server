import {Router, Request, Response} from 'express'
import coddyger, { IData, IErrorObject } from 'coddyger'
import { ICategory, CategorySet } from './';

const dao: IData<ICategory> = new CategorySet();
const defaultParents:any = [
	{
		_id: coddyger.string.generateObjectId(),
    title: 'Module Information',
    module: 'information',
    description: '',
    status: 'active',
    parent: null,
    user: null
	},
	{
		_id: coddyger.string.generateObjectId(),
    title: 'Module Actualité',
    module: 'actualite',
    description: '',
    status: 'active',
    parent: null,
    user: null
	},
	{
		_id: coddyger.string.generateObjectId(),
    title: 'Module Témoignage',
    module: 'temoignage',
    description: '',
    status: 'active',
    parent: null,
    user: null
	}
]

export class CategoryService {
	static sampleMethod(apikey: string, controllerLabel: string) {
		return new Promise(async (resolve, reject) => {

		}).catch((err: any) => {
			return coddyger.catchReturn(err, 'CategoryService', 'sampleMethod');
		});
	}

	static buildDefaults (): Promise<any> {
		return new Promise(async (resolve, reject) => {
			for(const parent of defaultParents) {
				const isParent:any = await dao.exist({title: parent.title})
				if(isParent === false) {
					parent.slug = coddyger.buildSlug('MOD')
					await dao.save(parent)
				} 
			}

			resolve(true)
		}).catch((err: any) => {
			return coddyger.catchReturn(err, 'CategoryService', 'buildDefaults');
		});
	}

	static isParentChildConflict (childId: string, newParentId: string): Promise<any> {
		return new Promise(async (resolve, reject) => {
			let currentId = newParentId;
			while (currentId) {
					if (currentId === childId) {
						resolve(true)
					}
					const parentCategory:any = await dao.selectOne({_id:currentId});
					if (!parentCategory) break;
					currentId = parentCategory.parent?.toString();
			}
			resolve(false)
		}).catch((err: any) => {
			return coddyger.catchReturn(err, 'CategoryService', 'isParentChildConflict');
		});
	}
}
