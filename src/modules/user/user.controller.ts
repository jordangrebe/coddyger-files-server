import coddyger, { IData, IErrorObject, defines, env } from 'coddyger'
import { locale } from "../../public";
import { ILogin, IUser, UserService, UserSet } from "./";
import { IProfile, ProfileSet } from '../profile';
import { MailerHelper, TokenHelper, UploadHelper, UserHelper } from '../../helpers';
import zxcvbn from 'zxcvbn';

const controllerLabel: string = 'UserController';
const upload:any = new UploadHelper

export class UserController {
	private readonly dao: IData<IUser>;
	private readonly daoProfile: IData<IProfile>

	constructor() {
		this.dao = new UserSet();
		this.daoProfile = new ProfileSet
	}

	// Login user to retrieve account information
	login(payload: ILogin) {
    return new Promise(async (resolve, reject) => {
      let login: string = payload.login;
      let password: string = payload.password;

      let params = {
				$or: [{ email: login }, { mobilePhone: login }]
			};
      // --- CHECKING UNIQUE KEY AVAILABILITY
      const exist:any = await this.dao.exist(params)

			if (exist.error) {
				reject(exist);
				return
			}

			if (exist === false) {
				resolve({
					status: defines.status.clientError,
					message: locale.user.controller.loginFailed,
					data: "notfound",
				});
			} else {
				let user: any = await this.dao.selectOne(params, "-__v");
				let verifyPassword:any = await coddyger.string.decryptPassword(password, user.password);

				if(verifyPassword === false) {
					resolve({
						status: defines.status.clientError,
						message: locale.user.controller.loginFailed,
						data: "wrongkey",
					});

					return
				}

				// Verify account status and generate access token

				if(user.status !== 'active') {
					resolve({
						status: defines.status.clientError,
						message: locale.user.controller.accountNotEnable,
						data: null,
					});
				} else {
					let jwt: any = TokenHelper.generate({ _id: user._id });
					delete user.password

					resolve({
						status: defines.status.requestOK,
						message: locale.user.controller.connected,
						data: {
							accessToken: jwt.accessToken, 
							refreshToken: jwt.refreshToken, 
							user
						},
					});
				}
			}
    }).catch((e: IErrorObject) => {
      return coddyger.catchReturn(e, controllerLabel, 'login')
    });
  }

	// Function to save User
	register(item: IUser) {
		return new Promise(async (resolve, reject) => {
			// Extracting necessary information from the item
			let username: any = item.username;
			let email: any = item.email;
			let password: any = item.password;
			let passwordConfirmation: any = item.passwordConfirmation;

			const passwordSScore: number = zxcvbn(password).score

			if (!coddyger.string.isEmailAddress(email)) {
				resolve({ status: defines.status.clientError, message: locale.user.controller.wrongEmailAddress, data: null });
			} else if (!UserService.isValidUsername(username)) {
				resolve({ status: defines.status.clientError, message: locale.user.controller.wrongUsername, data: null });
			} else if (passwordSScore <= 2) {
				resolve({ status: defines.status.clientError, message: locale.user.controller.passwordTooWeak, data: null });
			} else if (password !== passwordConfirmation) {
				resolve({ status: defines.status.clientError, message: locale.user.controller.passwordsMismatch, data: null });
			} else {
				const isEmail: any = await this.dao.exist({ email });
				const isUsername: any = await this.dao.exist({ username });

				// Validation checks for the item name
				if (isEmail) {
					resolve({ status: defines.status.clientError, message: locale.user.controller.takenEmail, data: null });
				} else if (isUsername) {
					resolve({ status: defines.status.clientError, message: locale.user.controller.takenUsername, data: null });
				} else {
					const profile:any = await this.daoProfile.selectOne({slug: "client"})
					password = await coddyger.string.encryptPassword(password);

					const theLast:any = await this.dao.selectLatest();
					// Generate a new ObjectId for the item
					item._id = coddyger.string.generateObjectId();
					item.slug = coddyger.buildSlug('USR', theLast ? theLast.slug : null);
					item.password = password
					item.profile = profile._id

					// Save the item to the local database
					const data: any | IErrorObject = await this.dao.save(item); 

					if (data.error) {
						// If there is an error saving to the local database, reject with the error response
						reject(data);
					} else {
						const mailTemplate:any = await MailerHelper.buildWelcomeTemplate(email)

						if(mailTemplate.error) {
							await this.dao.remove({ email })
							return reject(mailTemplate.data)
						}
						// Send mail with default password to user
						const sendMail:any = await MailerHelper.send({to: [email], subject: env.appName + ' - Données d\'authentification', html: mailTemplate})

						// Resolve with a success response and the updated item
						resolve({
							status: defines.status.requestOK,
							message: locale.controller.successSave,
							data: null
						});
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'register');
		});
	}

	save(item: IUser) {
		return new Promise(async (resolve, reject) => {
			// Extracting necessary information from the item
			let email: any = item.email;
			let profile: any = item.profile;
			let user: any = item.user;

			if (!coddyger.string.isValidObjectId(user)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId("du compte"), data: null });
			} else if (!coddyger.string.isValidObjectId(profile)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId("du profil"), data: null });
			} else {
				const isUser: any = await this.dao.exist({ _id:user });
				const isEmail: any = await this.dao.exist({ email });
				const isProfile: any = await this.dao.exist({ profile });

				// Validation checks for the item name
				if (isUser === false) {
					resolve({ status: defines.status.authError, message: locale.controller.notAdminAccount, data: null });
				} else if (isEmail) {
					resolve({
						status: defines.status.clientError,
						message: locale.exist('cette adresse e-mail'),
						data: null
					});
				} else if (!isProfile) {
					resolve({
						status: defines.status.clientError,
						message: locale.notfound('Profil'),
						data: null
					});
				} else {
					const passwordTemp:any = Math.floor(Math.random() * (999999 - 100000) ) + 100000;
					const password:any = await coddyger.string.encryptPassword(passwordTemp.toString());

					const theLast:any = await this.dao.selectLatest();
					// Generate a new ObjectId for the item
					item._id = coddyger.string.generateObjectId();
					item.slug = coddyger.buildSlug('ADM', theLast ? theLast.slug : null);
					item.password = password

					// Save the item to the local database
					const data: any | IErrorObject = await this.dao.save(item); 

					if (data.error) {
						// If there is an error saving to the local database, reject with the error response
						reject(data);
					} else {
						const mailTemplate:any = await MailerHelper.buildCredentialTemplate(item.firstname + ' '  + item.lastname, email, passwordTemp)

						if(mailTemplate.error) {
							await this.dao.remove({ email })
							return reject(mailTemplate.data)
						}
						// Send mail with default password to user
						const sendMail:any = await MailerHelper.send({to: [email], subject: env.appName + ' - Données d\'authentification', html: mailTemplate})
						// Retrieve the saved item from the database
						const updatedItem: any = await this.dao.selectOne({ _id: item._id }, '-password -__v');

						// Resolve with a success response and the updated item
						resolve({
							status: defines.status.requestOK,
							message: locale.controller.successSave,
							data: updatedItem
						});
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'save');
		});
	}

	// Function to update User
	update(item: IUser) {
		return new Promise(async (resolve, reject) => {
			let _id: string = item._id!;

			if (!coddyger.string.isValidObjectId(_id)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId("de l'enregistrement"), data: null });
			} else {
				// Controller l'existence de l'élément
				let isData: any = await this.dao.exist({ _id });

				if (isData.error) {
					reject(isData);
				} else if (isData === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Enregistrement'), data: null });
				} else {
					// Update the item with the history entry
					const update: any = await this.dao.update({ _id }, item);
					if (update.error) {
						reject(update);
					} else {
						// Retrieve the updated item from the database
						const updatedItem: any = await this.dao.selectOne({ _id }, '-password -__v');
						// Resolve with a success response and the updated item
						resolve({
							status: defines.status.requestOK,
							message: locale.controller.done,
							data: updatedItem
						});
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'update');
		});
	}

	updateStatus(item: IUser) {
		return new Promise(async (resolve, reject) => {
			let _id: string = item._id!;
			let status: string = item.status!;

			if (!coddyger.string.isValidObjectId(_id)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId("de l'enregistrement"), data: null });
			} else {
				// Controller l'existence de l'élément
				let isData: any = await this.dao.exist({ _id });

				if (isData.error) {
					reject(isData);
				} else if (isData === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Enregistrement'), data: null });
				} else {
					const statuses:Array<string> = ['active', 'suspended', 'disabled', 'removed', 'expired']

					let isStatus: any = statuses.includes(status);

					if (!isStatus) {
						resolve({ status: defines.status.clientError, message: locale.notfound('Statut'), data: null });
					} else {
						// Update the item with the history entry
						const update: any = await this.dao.update({ _id }, item);
						if (update.error) {
							reject(update);
						} else {
							// Retrieve the updated item from the database
							const updatedItem: any = await this.dao.selectOne({ _id }, '-password -__v');
							// Resolve with a success response and the updated item
							resolve({
								status: defines.status.requestOK,
								message: locale.controller.done,
								data: updatedItem
							});
						}
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'updateStatus');
		});
	}

	updatePassword(item: {
		user: string,
		password: string,
		passwordNew: string,
		passwordConf: string,
	}) {
		return new Promise(async (resolve, reject) => {
			let _id: string = item.user;
			let password: any = item.password;
			let passwordNew: string = item.passwordNew;
			let passwordConf: string = item.passwordConf;

			if (!coddyger.string.isValidObjectId(_id)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId("de l'enregistrement"), data: null });
			} else {
				// Controller l'existence de l'élément
				let isData: any = await this.dao.exist({ _id });

				if (isData.error) {
					reject(isData);
				} else if (isData === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Compte'), data: null });
				} else {
					const data:any = await this.dao.selectOne({_id})
					const currentPassword:string = data.password

					const passwordVerify:boolean = await coddyger.string.decryptPassword(password, currentPassword)

					if (!passwordVerify) {
						resolve({ status: defines.status.clientError, message: locale.user.controller.wrongPassword, data: null });
					} else if (passwordNew !== passwordConf) {
						resolve({ status: defines.status.clientError, message: locale.user.controller.passwordsMismatch, data: null });
					} else {
						password = await coddyger.string.encryptPassword(password)

						// Update the item with the history entry
						const update: any = await this.dao.update({ _id }, {password});
						if (update.error) {
							reject(update);
						} else {
							// Resolve with a success response and the updated item
							resolve({
								status: defines.status.requestOK,
								message: locale.controller.done,
								data: null
							});
						}
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'updatePassword');
		});
	}

	updateProfile(item: { user: string, profile: string }) {
		return new Promise(async (resolve, reject) => {
			let _id: string = item.user;
			let profile: any = item.profile;

			if (!coddyger.string.isValidObjectId(_id)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId("de l'enregistrement"), data: null });
			} else if (!coddyger.string.isValidObjectId(profile)) {
				resolve({ status: defines.status.clientError, message: locale.wrongObjectId("du profil"), data: null });
			} else {
				// Controller l'existence de l'élément
				let isData: any = await this.dao.exist({ _id });

				if (isData.error) {
					reject(isData);
				} else if (isData === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Compte'), data: null });
				} else {
					const isProfile:boolean| IErrorObject = await this.daoProfile.exist({profile})

					if (!isProfile) {
						resolve({ status: defines.status.clientError, message: locale.notfound('Profil'), data: null });
					} else {
						// Update the item with the history entry
						const update: any = await this.dao.update({ _id }, {profile});
						if (update.error) {
							reject(update);
						} else {
							// Resolve with a success response and the updated item
							resolve({
								status: defines.status.requestOK,
								message: locale.controller.done,
								data: null
							});
						}
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'updateProfile');
		});
	}

	// Function to remove User
	remove(_id: string) {
		return new Promise(async (resolve, reject) => {
			if (!coddyger.string.isValidObjectId(_id)) {
				resolve({ status: defines.status.clientError, message: locale.controller.wrongObjectId, data: null });
			} else {
				// Controller l'existence de l'élément
				let isData: any = await this.dao.exist({ _id });

				if (isData.error) {
					reject(isData);
				} else if (isData === false) {
					resolve({ status: defines.status.clientError, message: locale.notfound('Enregistrement'), data: null });
				} else {
					const remove: any = await this.dao.remove({ _id });

					if (remove.error) {
						reject(remove);
					} else {
						// Resolve with a success response and the updated demand item
						resolve({
							status: defines.status.requestOK,
							message: locale.controller.done,
							data: null
						});
					}
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'remove');
		});
	}

	// Function to select with parameters User
	select(payloads: { page?: number; pageSize?: number; query?: string; status?: string }) {
		return new Promise(async (resolve, reject) => {
			let page: number = payloads.page || 1;
			let pageSize: number = payloads.pageSize!;
			let query: string = payloads.query!;
			let status: any = payloads.status!;

			let data: any | IErrorObject = {};

			if (coddyger.string.isEmpty(query) && coddyger.string.isEmpty(status)) {
				data = await this.dao.select({ params: {}, page, pageSize });
			} else if (!coddyger.string.isEmpty(status)) {
				data = await this.dao.select({ params: { status }, page, pageSize });
			} else {
				if(query.includes(' ')) {
					const [firstname, lastname] = query.split(' ');

					data = await this.dao.select({
						params: {
							$or: [
								{ firstname: { $regex: firstname || '', $options: 'i' } },
								{ lastname: { $regex: lastname || '', $options: 'i' } },
							]
						},
						page,
						pageSize
					});
				} else if(query.includes('+')) {
					const [firstname, lastname] = query.split('+');

					data = await this.dao.select({
						params: {
							$or: [
								{ firstname: { $regex: firstname || '', $options: 'i' } },
								{ lastname: { $regex: lastname || '', $options: 'i' } },
							]
						},
						page,
						pageSize
					});
				} else {
					data = await this.dao.select({
						params: {
							$or: [
								{ slug: { $regex: query || '', $options: 'i' } },
								{ firstname: { $regex: query || '', $options: 'i' } },
								{ lastname: { $regex: query || '', $options: 'i' } },
							]
						},
						page,
						pageSize
					});
				}
			}

			if (data.error) {
				reject(data);
				return;
			}

			const rows: IUser[] = data.rows;
			delete data.rows;

			resolve({
				status: defines.status.requestOK,
				message: data,
				data: rows
			});
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'select');
		});
	}

	selectByRole(payloads: {role:string, page?: number; pageSize?: number; query?: string; status?: string }) {
		return new Promise(async (resolve, reject) => {
			let role: string = payloads.role;
			let page: number = payloads.page || 1;
			let pageSize: number = payloads.pageSize!;
			let query: string = payloads.query!;
			let status: any = payloads.status!;

			let data: any | IErrorObject = {};

			if (!coddyger.string.isEmpty(status)) {
				data = await this.dao.select({ params: { status, role }, page, pageSize });
			} else {
				if(query.includes(' ')) {
					const [firstname, lastname] = query.split(' ');

					data = await this.dao.select({
						params: {
							$and: [
								{
									$or: [
										{ firstname: { $regex: firstname || '', $options: 'i' } },
										{ lastname: { $regex: lastname || '', $options: 'i' } },
									]
								},
								{ role },
							]
						},
						page,
						pageSize
					});
				} else if(query.includes('+')) {
					const [firstname, lastname] = query.split('+');

					data = await this.dao.select({
						params: {
							$and: [
								{
									$or: [
										{ firstname: { $regex: firstname || '', $options: 'i' } },
										{ lastname: { $regex: lastname || '', $options: 'i' } },
									]
								},
								{ role },
							]
						},
						page,
						pageSize
					});
				} else {
					data = await this.dao.select({
						params: { role },
						page,
						pageSize
					});
				}
			}

			if (data.error) {
				reject(data);
				return;
			}

			const rows: IUser[] = data.rows;
			delete data.rows;

			resolve({
				status: defines.status.requestOK,
				message: data,
				data: rows
			});
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'select');
		});
	}

	// Function to select User by parameters
	selectByParams(params: any, page?: number) {
		return new Promise(async (resolve, reject) => {
			const data: any | IErrorObject = await this.dao.select({ params, page });
			if (data.error) {
				reject(data);
				return;
			}

			const rows: IUser[] = data.rows;
			delete data.rows;

			resolve({
				status: defines.status.requestOK,
				message: data,
				data: rows
			});
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'selectByParams');
		});
	}

	// Function to select all User
	selectAll() {
		return new Promise(async (resolve, reject) => {
			const data: any | IErrorObject = await this.dao.selectHug();
			if (data.error) {
				reject(data);
				return;
			}

			const rows: IUser[] = data;

			resolve({
				status: defines.status.requestOK,
				message: {
					totalRows: rows.length
				},
				data: rows || []
			});
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'selectAll');
		});
	}

	// Function to select detail of User by id
	selectOne(payload: string) {
		return new Promise(async (resolve, reject) => {
			if (!coddyger.string.isValidObjectId(payload)) {
				resolve({ status: defines.status.clientError, message: locale.controller.wrongObjectId, data: null });
			} else {
				const local: any | IErrorObject = await this.dao.selectOne({ _id: payload });

				if (local) {
					resolve({
						status: defines.status.requestOK,
						message: 'OK',
						data: local
					});
				} else {
					resolve({
						status: defines.status.notFound,
						message: locale.notfound('Enregistrement'),
						data: null
					});
					return;
				}
			}
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'selectOne');
		});
	}

	// Function to search User
	search(payload?: string, page?: number) {
		return new Promise(async (resolve, reject) => {
			const params = {
				$or: [
					{ slug: { $regex: new RegExp(payload!, 'i') } },
					{ title: { $regex: new RegExp(payload!, 'i') } },
					{ status: { $regex: new RegExp(payload!, 'i') } }
				]
			};

			const data: any | IErrorObject = await this.dao.select({ params, page });
			if (data.error) {
				reject(data);
				return;
			}

			const rows: IUser[] = data.rows;
			delete data.rows;

			resolve({
				status: defines.status.requestOK,
				message: data,
				data: rows
			});
		}).catch((e: IErrorObject) => {
			return coddyger.catchReturn(e, controllerLabel, 'search');
		});
	}

	generateToken(apikey:string) {
		return UserHelper.generateToken(apikey, controllerLabel)
	}
	verifyToken(jwtPayload:any) {
		return UserHelper.verifyToken(jwtPayload, controllerLabel)
	}
}
