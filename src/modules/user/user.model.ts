import mongoose, { Schema, Document } from 'mongoose';
import coddyger, { IData, defines, LoggerService, LogLevel, MongoDbDao } from 'coddyger'

export interface IUser {
	_id?: string; // MongoDB generated id
  slug?: string,
  email?: string
  username?: string
  password?: any
  passwordConfirmation?: any
  lastname?: string,
  firstname?: string,
  role?: string,
  contact?: string,

  lastLoginDate?: Date,
  expireDate?: Date,
  expiredOnDate?: Date,
  isEmailConfirmed?:boolean,

  status?: string, //
  profile?: any,
  user?: any,
}

export interface IRegister {
	email: string,
	password: string,
	passwordConf: string,
}

export interface ILogin {
	login: string,
	password: string,
}

const schema = new mongoose.Schema<IUser>(
	{
		_id: Schema.Types.ObjectId,
		slug: String,
		email: { type: String },
		username: { type: String },
		password: { type: String },
		lastname: String,
		firstname: String,
		role: { type: String, enum: ['admin', 'user'], default: 'admin'},
		contact: String,
		lastLoginDate: { type: Date },
		expireDate: { type: Date },
		expiredOnDate: { type: Date },
		status: { type: String, enum: ['active', 'suspended', 'disabled', 'removed', 'expired'], default: 'active' },
		isEmailConfirmed: { type: Boolean, default: false },
		profile: { type: mongoose.Schema.Types.ObjectId, ref: 'Profile' },
		user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
	},
	{ timestamps: true }
);

const model = mongoose.model<IUser>('User', schema);

export class UserSet extends MongoDbDao<Document> implements IData<Document> {
	defaultModel = model;

	constructor() {
		super();
	}

	props:string = "email firstname lastname user"
	userProps: string = 'slug email lastname firstname';
	setTitle: string = 'UserSet';

	select(payloads: { 
		params?: any, 
		excludes?: string, 
		page?: number, 
		pageSize?: number,
		sort?: string,
		orderBy?: string; 
	}): Promise<any> {
		return new Promise(async (resolve, reject) => {
			if (coddyger.string.isEmpty(payloads.params?.status)) {
				payloads.params = { ...payloads.params, status: { $nin: ['removed', 'archived'] } };
			}

			let page: number = Number(payloads.page) || 1;
			const pageSize: number = Number(payloads.pageSize) || 10;

			page = page === 0 ? 1 : page;

			const startIndex = (page - 1) * pageSize;
			const model = this.defaultModel;
			let sortBy: string = payloads.sort ?? 'createdAt';
			let orderBy: string = payloads.orderBy ?? 'desc';

			// Create sort object
			let sortObject: any = {};
			if (sortBy) {
				sortObject = {
					[sortBy]: orderBy === 'desc' ? -1 : 1
				};
			} else {
				// Default sort by createdAt in descending order if no sortBy is provided
				sortObject = { createdAt: -1 };
			}

			const [rows, totalRows] = await Promise.all([
				model
					.find(payloads.params, "-password -__v")
					.sort(sortObject)
					.skip(startIndex)
					.limit(pageSize)
					.lean(),
				model.countDocuments(payloads.params)
			]);

			if (rows) {
				const totalPages = Math.ceil(totalRows / pageSize);
				const countRowsPerPage = rows.length;
				const totalPagesPerQuery = Math.ceil(totalRows / pageSize);

				resolve({
					rows,
					totalRows,
					totalPages,
					countRowsPerPage,
					totalPagesPerQuery
				});
			} else {
				reject({ rows, totalRows });
			}
		}).catch((e: any) => {
			LoggerService.log({
				type: LogLevel.Error,
				content: JSON.stringify(e),
				location: this.setTitle,
				method: 'select'
			});
			return { error: true, data: e, message: defines.message.tryCatch };
		});
	}

	selectHug(): Promise<Array<Document> | any> {
		return new Promise(async (resolve, reject) => {
			let doc = await this.defaultModel
				.find()
				.lean()
				.populate({
					path: 'user',
					select: this.props,
					options: { sort: { createdAt: -1 } }
				}).populate({
					path: 'profile',
					populate: {
						path: 'user',
						select: this.props,
					},
					options: { sort: { createdAt: -1 } }
				});

			if (!doc) {
				reject(doc);
			} else {
				resolve(doc);
			}
		}).catch((e: any) => {
			LoggerService.log({ type: LogLevel.Error, content: e, location: this.setTitle, method: 'selectHug' });
			return { error: true, data: e, message: defines.message.tryCatch };
		});
	}

	selectOne(params: object, fields?: string): Promise<Document | any> {
		return new Promise(async (resolve, reject) => {
			resolve(
				await this.defaultModel
					.findOne(params, fields)
					.lean()
					.populate({
						path: 'user',
						select: this.props,
						options: { sort: { createdAt: -1 } }
					}).populate({
						path: 'profile',
						populate: {
							path: 'user',
							select: this.props,
						},
						options: { sort: { createdAt: -1 } }
					})
			);
		}).catch((e: any) => {
			LoggerService.log({ type: LogLevel.Error, content: e, location: this.setTitle, method: 'selectOne' });
			return { error: true, data: e, message: defines.message.tryCatch };
		});
	}
}