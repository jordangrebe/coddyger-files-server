import {Router, Request, Response} from 'express'
import coddyger, { IData, IErrorObject } from 'coddyger'
import { IUser, UserSet } from './';

const dao: IData<IUser> = new UserSet();

export class UserService {
	static sampleMethod(apikey: string, controllerLabel: string) {
		return new Promise(async (resolve, reject) => {

		}).catch((err: any) => {
			return coddyger.catchReturn(err, 'UserService', 'sampleMethod');
		});
	}

	static isValidUsername(username: string): boolean {
		const regex = /^[a-zA-Z][a-zA-Z0-9_-]{2,15}$/;
  	return regex.test(username);
	}

	static isValidPassword(payload: string): boolean {
		const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
		return passwordRegex.test(payload);
	}
}
