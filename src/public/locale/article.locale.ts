export const ArticleLocale = {
	router: {
		idRequired: "La référence de l'objet est requise",
		titleRequired: 'Le titre de l\'article est requis',
		statusRequired: 'Le statut est requis'
	},
	controller: {
		successSave: 'Création de compte réussie!',
		successUpdate: 'Modification réussie!',
		successRemove: 'Enregistrement supprimé avec succès.'
	}
};
