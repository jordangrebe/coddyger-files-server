export const CategoryLocale = {
	router: {
		idRequired: "La référence de l'objet est requise",
		titleRequired: 'Le libellé est requis',
		websiteRequired: 'Le site est requis',
		statusRequired: 'Le statut est requis'
	},
	controller: {
		successSave: 'Création de compte réussie!',
		successUpdate: 'Modification réussie!',
		successRemove: 'Enregistrement supprimé avec succès.',
		cantBeOwnParent: 'Une categorie ne peut s\'avoir comme parent.',
		parentChildConflict: 'Impossible de définir une catégorie comme parent de son enfant.',
	}
};
