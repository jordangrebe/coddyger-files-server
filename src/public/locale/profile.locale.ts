export const ProfileLocale = {
	router: {
		idRequired: "La référence de l'objet est requise",
		titleRequired: 'Le libellé du profil est requis',
		abilityRequired: 'Veuiller ajouter une ou plusieurs abilitations',
		statusRequired: 'Le statut est requis'
	},
	controller: {
		successSave: 'Création de compte réussie!',
		successUpdate: 'Modification réussie!',
		successRemove: 'Enregistrement supprimé avec succès.'
	}
};
