export const UserLocale = {
	router: {
		idRequired: "La référence de l'objet est requise",
		nomRequired: 'Le nom est requis',
		prenomsRequired: 'Le prénom est requis',
		apikeyRequired: "La clé d'api est requise",
		emailRequired: "L'adresse e-mail est requise",
		usernameRequired: "Le nom d'utilisateur est requis",
		passwordRequired: 'Le mot de passe est requis',
		passwordConfirmationRequired: 'la confirmation du mot de passe est requise',
		profileRequired: 'Le profil est requis',
		rolesRequired: 'Un ou plusieurs roles sont requis',
		statusRequired: 'Le statut est requis'
	},
	controller: {
		notFound: 'Enregistrement introuvable',
		notAuthorized: 'Accès non autorisé',
		notAdminAccount: "Ce compte n'est pas administrateur",
		wrongObjectId: "L'identifiant de l'objet n'est pas valide",
		saverNotfound:
			"Impossible de vérifier l'identité de l'administrateur, veuillez vous reconnecter. Si le problème persiste contactez un administrateur système.",
		subcategoryNotfound: 'Sous-categorie introuvable',
		successSave: 'Création de compte réussie!',
		successUpdate: 'Modification réussie!',
		successRemove: 'Enregistrement supprimé avec succès.',
		successMassiveRemove: 'Enregistrement supprimés avec succès.',
		apiKeyNotFound: 'Action non autorisée',
		session: {
			ok: 'Session ok',
			error: 'Session invalide'
		},
		done: 'Action éffectuée avec succès',
		notValidStatus: 'Statut invalide',
		notValidEtape: 'Etape invalide',
		sameStatusDetected: 'Le statut sélectionné est déjà celui du dossier en cours',
		wrongStatusDetected: "Le statut sélectionné n'est pas celui attendu pour le traitement",
		isReadonly: 'Ce dossier ne peut être édité',
		filesXdocsNotGood: 'Le nombre de fichier sélectionné ne correspond pas au nombre de type de dossier',
		filesRequired: 'Vous devez ajouter au moins un document pour la demande',
		wrongEmailAddress: 'Adresse e-mail invalide',
		wrongUsername: "Nom d'utilisateur invalide",
		passwordsMismatch: 'Le mot de passe et la confirmation sont différents',
		passwordTooShort: 'Mot de passe trop court',
		passwordTooWeak: 'Mot de passe trop faible',
		wrongPassword: 'Mot de passe incorrecte',
		noPassword: 'Veuiller créer un mot de passe',
		takenEmail: 'Adresse e-mail indisponible',
		takenUsername: 'Pseudo indisponible',
		loginFailed: 'Identifiant ou mot de passe incorrect',
		accountNotEnable: 'Compte inactif, merci de contacter votre administrateur système',
		connected: 'Connexion éffectuée!',
		disconnected: 'Déconnexion éffectuée!',
		unknownDomain: "L'adresse e-mail n'a pas un domaine valide",
		direction: {
			required: "Veuillez sélectionner une direction pour ce compte",
		},
		site: {
			required: "Veuillez sélectionner une direction pour ce compte",
		},
		up: {
			required: "Veuillez sélectionner une unité de production pour ce compte",
		}
	}
}
