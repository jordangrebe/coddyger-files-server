export const WebsiteLocale = {
	router: {
		idRequired: "La référence de l'objet est requise",
		titleRequired: 'Le libellé est requis',
		urlRequired: 'L\'URL du site est requis',
		apikeyRequired: 'L\'apikey est requis',
		statusRequired: 'Le statut est requis'
	},
	controller: {
		successSave: 'Création de compte réussie!',
		successUpdate: 'Modification réussie!',
		successRemove: 'Enregistrement supprimé avec succès.'
	}
};
