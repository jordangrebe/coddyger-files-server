import express, { Request, Response, NextFunction } from 'express';
import helmet from 'helmet';
import cors from 'cors';
import morgan from "morgan";

import coddyger, { defines, env } from "coddyger";

export default class RouterService {
  // Un tableau pour enregistrer les routeurs
  static routesRegister = new Array<any>();

  // Méthode pour exécuter la configuration, les middleware "before" et "after", et définir les routes
  static run(app) {
    this.config(app); // Configuration des prérequis
    this.before(app); // Middleware "before"
    this.setRoutes(app); // Définition des routes

  }

  // Décorateur de route, ajoute un routeur au tableau routesRegister
  static routeDecorator = (router: any) => {
    return (target: any, property: string) => {
      target[property] = router;
      this.routesRegister.push(router);
    }
  };

  // Méthode pour définir les routes sur l'application Express
  static setRoutes(app) {
    let path = env.server.serverPath + '/' + env.server.apiVersion;

    if (this.routesRegister.length >= 1) {
      app.use(path, this.routesRegister);
    } else {
      console.log('===> Aucune route trouvée');
    }

		// app.use('/docs', swaggerUi.serve, swaggerUi.setup(specs));

		// Middleware de gestion des erreurs pour les routes introuvables
    app.use((req, res, next) => {
      res.status(defines.status.notFound).json({
        message: defines.message.notFound,
        data: null
      });
    });
  }

  // Middleware "before" pour gérer les erreurs de requête
  static before(app) {
    app.use((err: any, req: Request, res: Response, next: NextFunction) => {
      if (err && err.status === defines.status.badRequest && 'body' in err) {
				return coddyger.api(res, new Promise((resolve) => {
					resolve({
							status: defines.status.badRequest,
							message: defines.message.badRequest,
							data: null
					});
				}));
			}

      // Passe à l'étape suivante du middleware
      next();
    });
  }

  // Configuration des prérequis pour l'application
  private static config(app): void {
    app.use(helmet()); // Middleware de sécurité
    app.use(cors()); // Middleware de gestion des CORS
    app.use(express.urlencoded({ extended: true })); // Middleware pour gérer les données URL encodées
    app.use(express.json()); // Middleware pour analyser les données JSON
		app.use(morgan("tiny"));
    app.use(express.static('public/uploads')); // Middleware pour servir des fichiers statiques
  }
}
