import {Router, Request, Response} from 'express'
import coddyger from 'coddyger'
import RouterService from '../router';
import { ArticleController } from '../modules/article/';
import Validator, { articleValidator } from '../middlewares/validators';
import { TokenHelper, UploadHelper } from '../helpers';
import FileHelper from '../helpers/file.helper';

const route = Router();
const Controller: ArticleController = new ArticleController();
const routePath = '/article';

route.post(`${routePath}/save-step-1`, TokenHelper.verify, articleValidator.saveStepOne(), Validator.validate, (req: any, res: Response) => {
	let body: any = req.body;

	let user: any = req.user;
	body.user = user._id;

	let Q = Controller.saveStepOne(body);
	return coddyger.api(res, Q);
});

route.put(`${routePath}/join-image`, TokenHelper.verify, UploadHelper.single, (req: any, res: Response) => {
	let body: any = req.body;

	const user: any = req.user;
	const file:any = req.file;

	body.image = file
	body.user = user._id;

	let Q = Controller.joinImage(body);
	return coddyger.api(res, Q);
});

route.put(`${routePath}/join-audio`, TokenHelper.verify, (req: any, res: Response) => {
	// Set up the file uploader
	const uploadDirectory = '';
	const maxSize = 5 * 1024 * 1024; // 5 MB
	const fileTypes = ['.png', '.jpg', '.jpeg', '.gif', '.mp3', '.wav', '.ogg']; // Added audio file extensions

	const fileUploader = new FileHelper(uploadDirectory, maxSize, fileTypes);

	fileUploader.uploadFile(req, res, () => {
		let body: any = req.body;
		const user: any = req.user;
		body.user = user._id;
		body.audio = req.file;

		let Q = Controller.joinAudio(body);
		return coddyger.api(res, Q);
	});
});


route.post(`${routePath}/upload-file`, TokenHelper.verifyAuth, (req: any, res: Response) => {
	let body: any = req.body;
	// Set up the file uploader
	const uploadDirectory = '';
	const maxSize = 5 * 1024 * 1024; // 5 MB
	const fileTypes = ['pdf', 'xls', 'xlsx', 'docx', 'doc', 'png', 'jpg', 'jpeg']; // Added audio file extensions

	const fileUploader = new FileHelper(uploadDirectory, maxSize, fileTypes);

	fileUploader.uploadFile(req, res, () => {
		const user: any = req.user;
		body.user = user._id;
		body.file = req.file;

		let Q = Controller.uploadFile(body);
		return coddyger.api(res, Q);
	});
});

route.post(`${routePath}/save`, TokenHelper.verify, (req: any, res: Response) => {
	let body: any = req.body;

	let user: any = req.user;
	body.user = user._id;

	let Q = Controller.save(body);
	return coddyger.api(res, Q);
});

route.put(`${routePath}/update`, TokenHelper.verify, (req: any, res: Response) => {
	let body: any = req.body;

	let user: any = req.user;
	body.user = user._id;

	let Q = Controller.update(body);
	return coddyger.api(res, Q);
});

route.put(`${routePath}/update/status`, TokenHelper.verify, (req: any, res: Response) => {
	let body: any = req.body;

	let user: any = req.user;
	body.user = user._id;

	let Q = Controller.updateStatus(body);
	return coddyger.api(res, Q);
});

route.delete(`${routePath}/remove/:payload`, TokenHelper.verify, (req: any, res: Response) => {
	let payload: string = req.params.payload;
	// let user:any = req.user;

	let Q = Controller.remove(payload);
	return coddyger.api(res, Q);
});

route.put(`${routePath}/restore/:payload`, TokenHelper.verify, (req: any, res: Response) => {
	let payload: string = req.params.payload;
	// let user:any = req.user;

	let Q = Controller.restore(payload);
	return coddyger.api(res, Q);
});

route.get(`${routePath}/select`, TokenHelper.verify, (req: any, res: Response) => {
	let page: any = req.query.page || 1;
	let pageSize: any = req.query.pageSize;
	let status: any = req.query.status;
	let query: any = req.query.q;
	let module: any = req.query.module;
	const user: any = req.user._id

	let Q = Controller.select({ page, pageSize, status, query, user, module });
	return coddyger.api(res, Q);
});

route.get(`${routePath}/select-all`, TokenHelper.verify, (req: any, res: Response) => {
	let Q = Controller.selectAll();
	return coddyger.api(res, Q);
});

route.get(`${routePath}/select-one/:payload`, TokenHelper.verify, (req: any, res: Response) => {
	let payload: string = req.params.payload;

	let Q = Controller.selectOne(payload);
	return coddyger.api(res, Q);
});

route.get(`${routePath}/select/:payload`, TokenHelper.verify, (req: any, res: Response) => {
	let page: any = req.query.page || 1;

	let Q = Controller.select(page);
	return coddyger.api(res, Q);
});

route.get(`${routePath}/search`, TokenHelper.verify, (req: any, res: Response) => {
	let payload: any = req.query.payload;
	let page: any = req.query.page || 1;

	let Q = Controller.search(payload, page);
	return coddyger.api(res, Q);
});

export class ArticleRoute {
	@RouterService.routeDecorator(route)
	static router: any;
	constructor() {}
}
