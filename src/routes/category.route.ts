import {Router, Request, Response} from 'express'
import coddyger from 'coddyger'
import RouterService from '../router';
import { CategoryController } from '../modules/category/';
import { TokenHelper } from '../helpers';
import Validator, { categoryValidator } from '../middlewares/validators';

const route = Router();
const Controller: CategoryController = new CategoryController();
const routePath = '/category';

route.post(`${routePath}/save`, TokenHelper.verify, categoryValidator.save(), Validator.validate, (req: any, res: Response) => {
	let body: any = req.body;

	let user: any = req.user;
	body.user = user._id;

	let Q = Controller.save(body);
	return coddyger.api(res, Q);
});

route.put(`${routePath}/update`, TokenHelper.verify, categoryValidator.update(), Validator.validate, (req: any, res: Response) => {
	let body: any = req.body;

	let user: any = req.user;
	body.user = user._id;

	let Q = Controller.update(body);
	return coddyger.api(res, Q);
});

route.delete(`${routePath}/remove/:payload`, TokenHelper.verify, (req: any, res: Response) => {
	let payload: string = req.params.payload;
	let user:any = req.user;

	let Q = Controller.remove(payload);
	return coddyger.api(res, Q);
});

route.put(`${routePath}/restore/:payload`, TokenHelper.verify, (req: any, res: Response) => {
	let payload: string = req.params.payload;
	let user:any = req.user;

	let Q = Controller.restore(payload);
	return coddyger.api(res, Q);
});

route.get(`${routePath}/select`, TokenHelper.verify, (req: any, res: Response) => {
	let page: any = req.query.page || 1;
	let pageSize: any = req.query.pageSize;
	let status: any = req.query.status;
	let query: any = req.query.q;
	const user: any = req.user._id

	let Q = Controller.select({ page, pageSize, status, query, user });
	return coddyger.api(res, Q);
});

route.get(`${routePath}/select-all`, TokenHelper.verify, (req: any, res: Response) => {
	const user: any = req.user._id
	let Q = Controller.selectHug({user});
	return coddyger.api(res, Q);
});

route.get(`${routePath}/select-parents`, TokenHelper.verify, (req: any, res: Response) => {
	const user: any = req.user._id

	let Q = Controller.selectHug({
		$or: [
			{ slug: { $regex: '^MOD' } },
			{ $and: [{ parent: null }, { user }] }
		]
	});
	return coddyger.api(res, Q);
});

route.get(`${routePath}/select-by-parent/:parent`, TokenHelper.verify, (req: any, res: Response) => {
	const user: any = req.user._id
	let parent: string = req.params.parent;

	let Q = Controller.selectByParent(parent, user);
	return coddyger.api(res, Q);
});


route.get(`${routePath}/select-one/:payload`, TokenHelper.verify, (req: any, res: Response) => {
	let payload: string = req.params.payload;

	let Q = Controller.selectOne(payload);
	return coddyger.api(res, Q);
});

export class CategoryRoute {
	@RouterService.routeDecorator(route)
	static router: any;
	constructor() {}
}
