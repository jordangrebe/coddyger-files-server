import coddyger from 'coddyger';

const exportedFiles: any = coddyger.filesInclude(__dirname, 'route');

// Exportez toutes les classes de route découvertes
export { exportedFiles };
