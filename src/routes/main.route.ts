import { Router } from 'express';
import coddyger from 'coddyger';
import path from 'path';
import fs from 'fs';
import RouterService from '../router';
import { MainHelper, UploadHelper } from '../helpers';
import mime from 'mime';


const route = Router();
// Read package.json file
const packageJsonPath = path.join(__dirname, '..', '..', 'package.json');
const packageJson = JSON.parse(fs.readFileSync(packageJsonPath, 'utf8'));

route.get('/container', (req: any, res: any) => {
	let Q = new Promise(async (resolve, reject) => {
		resolve({
			status: 201,
			message: 'Service is Up',
			data: {
				name: packageJson.name,
				version: packageJson.version,
				description: packageJson.description,
			}
		});
	});
	return coddyger.api(res, Q);
});

route.get('/select-file/:filename', async (req : any, res : any) => {
	let filename: string = req.params.filename;
	let filePath = UploadHelper.uploadPath + filename;

	const mimeType:any = mime.getType(filePath);

	fs.exists(filePath, function (exist: any) {
		if (!exist) {
			// if the file is not found, return 404
			res.statusCode = 404;
			res.end(`Fichier ${filePath} introuvable!`);
			return;
		}

		// if is a directory, then look for index.html
		if (fs.statSync(filePath).isDirectory()) {
			filePath += '/index.html';
		}

		// read file from file system
		fs.readFile(filePath, function (err, data) {
			if (err) {
				res.statusCode = 500;
				res.end(`Error getting the file: ${err}.`);
			} else {
				// if the file is found, set Content-type and send data
				res.setHeader('Content-type', mimeType);
				res.setHeader('Cross-Origin-Resource-Policy', 'cross-origin');
				res.end(data);
			}
		});
	});
});

export class MainRoute {
	@RouterService.routeDecorator(route)
	static router: any;
	constructor() {}
}
