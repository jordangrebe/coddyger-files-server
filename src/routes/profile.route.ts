import {Router, Request, Response} from 'express'
import coddyger from 'coddyger'
import RouterService from '../router';
import { ProfileController } from '../modules/profile/';
import { TokenHelper } from '../helpers';
import Validator, { profileValidator } from '../middlewares/validators';

const route = Router();
const Controller: ProfileController = new ProfileController();
const routePath = '/profile';

route.post(`${routePath}/save`, TokenHelper.verify, profileValidator.save(), Validator.validate, (req: any, res: Response) => {
	let body: any = req.body;

	let user: any = req.user;
	body.user = user._id;

	let Q = Controller.save(body);
	return coddyger.api(res, Q);
});

route.put(`${routePath}/update`, TokenHelper.verify, profileValidator.update(), Validator.validate, (req: any, res: Response) => {
	let body: any = req.body;

	let user: any = req.user;
	body.user = user._id;

	let Q = Controller.update(body);
	return coddyger.api(res, Q);
});

route.delete(`${routePath}/remove/:payload`, TokenHelper.verify, (req: any, res: Response) => {
	let payload: string = req.params.payload;
	// let user:any = req.user;

	let Q = Controller.remove(payload);
	return coddyger.api(res, Q);
});

route.put(`${routePath}/restore/:payload`, TokenHelper.verify, (req: any, res: Response) => {
	let payload: string = req.params.payload;
	// let user:any = req.user;

	let Q = Controller.restore(payload);
	return coddyger.api(res, Q);
});

route.get(`${routePath}/select`, TokenHelper.verify, (req: any, res: Response) => {
	let page: any = req.query.page || 1;
	let pageSize: any = req.query.pageSize;
	let status: any = req.query.status;
	let query: any = req.query.q;

	let Q = Controller.select({ page, pageSize, status, query });
	return coddyger.api(res, Q);
});

route.get(`${routePath}/select-all`, TokenHelper.verify, (req: any, res: Response) => {
	let Q = Controller.selectAll();
	return coddyger.api(res, Q);
});

route.get(`${routePath}/select-one/:payload`, TokenHelper.verify, (req: any, res: Response) => {
	let payload: string = req.params.payload;

	let Q = Controller.selectOne(payload);
	return coddyger.api(res, Q);
});

route.get(`${routePath}/search`, TokenHelper.verify, (req: any, res: Response) => {
	let payload: any = req.query.payload;
	let page: any = req.query.page || 1;

	let Q = Controller.search(payload, page);
	return coddyger.api(res, Q);
});

export class ProfileRoute {
	@RouterService.routeDecorator(route)
	static router: any;
	constructor() {}
}
