import {Router, Request, Response} from 'express'
import coddyger from 'coddyger'
import RouterService from '../router';
import { UserController } from '../modules/user/';
import { TokenHelper } from '../helpers';
import Validator, { userValidator } from '../middlewares/validators';

const route = Router();
const Controller: UserController = new UserController();
const routePath = '/user';

route.post(`${routePath}/get-token`, (req : any, res : Response) => {
	let apikey:string = req.body.apikey

  let Q = Controller.generateToken(apikey)
  return coddyger.api(res, Q);
});

route.post(`${routePath}/check-token`, TokenHelper.verify, (req : any, res : Response) => {
	let user:any = req.user

  let Q = Controller.verifyToken(user)
  return coddyger.api(res, Q);
});

route.post(`${routePath}/login`, TokenHelper.verifyAuth, userValidator.login(), Validator.validate, (req : any, res : any) => {
	let body: any = req.body

	let Q = Controller.login(body)

	return coddyger.api(res, Q);
});

route.post(`${routePath}/register`, TokenHelper.verifyAuth, userValidator.register(), Validator.validate, (req : any, res : any) => {
	let body: any = req.body

	let Q = Controller.register(body)

	return coddyger.api(res, Q);
});

route.post(`${routePath}/save`, TokenHelper.verify, (req: any, res: Response) => {
	let body: any = req.body;

	let user: any = req.user;
	body.user = user._id;

	let Q = Controller.save(body);
	return coddyger.api(res, Q);
});

route.put(`${routePath}/update`, TokenHelper.verify, (req: any, res: Response) => {
	const selfSave:string = req.query.selfSave
	let body: any = req.body;

	let user: any = req.user;
	body.user = user._id;

	// Si selfSave égale yes on considère que la modification vient du propriétaire du compte
	if(selfSave === "yes") {
		body._id = user._id
	}

	let Q = Controller.update(body);
	return coddyger.api(res, Q);
});

route.put(`${routePath}/update/status`, TokenHelper.verify, (req: any, res: Response) => {
	const selfSave:string = req.query.selfSave
	let body: any = req.body;

	let user: any = req.user;
	body.user = user._id;

	// Si selfSave égale yes on considère que la modification vient du propriétaire du compte
	if(selfSave === "yes") {
		body._id = user._id
	}

	let Q = Controller.updateStatus(body);
	return coddyger.api(res, Q);
});

route.put(`${routePath}/update/password`, TokenHelper.verify, (req: any, res: Response) => {
	const selfSave:string = req.query.selfSave
	let body: any = req.body;

	let user: any = req.user;
	body.user = user._id;

	let Q = Controller.updatePassword(body);
	return coddyger.api(res, Q);
});

route.put(`${routePath}/update/profile`, TokenHelper.verify, (req: any, res: Response) => {
	const selfSave:string = req.query.selfSave
	let body: any = req.body;

	let user: any = req.user;
	body.user = user._id;

		// Si selfSave égale yes on considère que la modification vient du propriétaire du compte
		if(selfSave === "yes") {
			body._id = user._id
		}

	let Q = Controller.updatePassword(body);
	return coddyger.api(res, Q);
});

route.delete(`${routePath}/remove/:payload`, TokenHelper.verify, (req: any, res: Response) => {
	let payload: string = req.params.payload;
	// let user:any = req.user;

	let Q = Controller.remove(payload);
	return coddyger.api(res, Q);
});

route.get(`${routePath}/select`, TokenHelper.verify, (req: any, res: Response) => {
	let page: any = req.query.page || 1;
	let pageSize: any = req.query.pageSize;
	let status: any = req.query.status;
	let query: any = req.query.q;

	let Q = Controller.select({ page, pageSize, status, query });
	return coddyger.api(res, Q);
});

route.get(`${routePath}/select-all`, TokenHelper.verify, (req: any, res: Response) => {
	let Q = Controller.selectAll();
	return coddyger.api(res, Q);
});

route.get(`${routePath}/select-one/:payload`, TokenHelper.verify, (req: any, res: Response) => {
	let payload: string = req.params.payload;

	let Q = Controller.selectOne(payload);
	return coddyger.api(res, Q);
});

route.get(`${routePath}/select/role`, TokenHelper.verify, (req: any, res: Response) => {
	let role: any = req.query.role;
	let page: any = req.query.page || 1;
	let pageSize: any = req.query.pageSize;
	let status: any = req.query.status;
	let query: any = req.query.q;

	let Q = Controller.selectByRole({ role, page, pageSize, status, query });
	return coddyger.api(res, Q);
});

route.get(`${routePath}/search`, TokenHelper.verify, (req: any, res: Response) => {
	let payload: any = req.query.payload;
	let page: any = req.query.page || 1;

	let Q = Controller.search(payload, page);
	return coddyger.api(res, Q);
});

export class UserRoute {
	@RouterService.routeDecorator(route)
	static router: any;
	constructor() {}
}
